package com.quickcash.mvp.profile;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.quickcash.R;
import com.quickcash.common.helpers.DateFormat;
import com.quickcash.common.interfaces.OnClickInterface;
import com.quickcash.common.utility.SnackNotify;
import com.quickcash.common.utility.Utils;
import com.quickcash.mvp.common_mvp.DateFragment;
import com.quickcash.mvp.navigation.NavigationActivity;
import com.quickcash.mvp.profile.presenter.ProfileContractor;
import com.quickcash.mvp.profile.presenter.ProfilePresenterImpl;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ProfileFragment extends Fragment implements ProfileContractor.ProfileView {

    @BindView(R.id.frameLayout)
    FrameLayout frameLayout;

    @BindView(R.id.txtViewMyProfileTitle)
    AppCompatTextView txtViewMyProfileTitle;

    @BindView(R.id.txtViewFirstNameTitle)
    AppCompatTextView txtViewFirstNameTitle;

    @BindView(R.id.edtTextFirstName)
    AppCompatEditText edtTextFirstName;

    @BindView(R.id.imgViewFirstName)
    ImageView imgViewFirstName;


    @BindView(R.id.txtViewLastNameTitle)
    AppCompatTextView txtViewLastNameTitle;

    @BindView(R.id.edtTextLastName)
    AppCompatEditText edtTextLastName;

    @BindView(R.id.imgViewLastName)
    ImageView imgViewLastName;


    @BindView(R.id.txtViewDOBTitle)
    AppCompatTextView txtViewDOBTitle;

    @BindView(R.id.txtViewDob)
    AppCompatTextView txtViewDob;

    @BindView(R.id.imgViewDOB)
    ImageView imgViewDOB;


    @BindView(R.id.txtViewAddress)
    AppCompatTextView txtViewAddress;

    @BindView(R.id.edtTextAddress)
    AppCompatEditText edtTextAddress;

    @BindView(R.id.imgViewAddress)
    ImageView imgViewAddress;


    @BindView(R.id.txtViewPhoneNumber)
    AppCompatTextView txtViewPhoneNumber;

    @BindView(R.id.edtTextPhoneNumber)
    AppCompatEditText edtTextPhoneNumber;

    @BindView(R.id.imgViewPhoneNumber)
    ImageView imgViewPhoneNumber;


    @BindView(R.id.txtViewEmail)
    AppCompatTextView txtViewEmail;

    @BindView(R.id.edtTextEmail)
    AppCompatEditText edtTextEmail;

    @BindView(R.id.imgViewEmail)
    ImageView imgViewEmail;


    @BindView(R.id.txtViewSalary)
    AppCompatTextView txtViewSalary;

    @BindView(R.id.edtTextSalary)
    AppCompatEditText edtTextSalary;

    @BindView(R.id.imgViewSalary)
    ImageView imgViewSalary;


    @BindView(R.id.txtViewPanCard)
    AppCompatTextView txtViewPanCard;

    @BindView(R.id.edtTextPanCardNo)
    AppCompatEditText edtTextPanCardNo;

    @BindView(R.id.imgViewPanCard)
    ImageView imgViewPanCard;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    ProfilePresenterImpl profilePresenter;
    ArrayList<ProfileModel.Datum> arrayListProfile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        profilePresenter = new ProfilePresenterImpl(this, getActivity());

        getProfileDataApi();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, frameLayout);
    }

    @Override
    public void onInternetError() {


        SnackNotify.checkConnection(onretryGetProfileApi, frameLayout);
    }

    OnClickInterface onretryGetProfileApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getProfileDataApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, frameLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, frameLayout);
    }

    @Override
    public void getProfileSuccess(ArrayList<ProfileModel.Datum> arrayListProfileModel) {
        arrayListProfile = arrayListProfileModel;
        setField();
    }


    @Override
    public void onSaveSuccess(String message) {
        showAlertDialog(message);

        //enabled the field to false
        edtTextFirstName.setEnabled(false);
        edtTextLastName.setEnabled(false);
        txtViewDob.setEnabled(false);
        edtTextAddress.setEnabled(false);
        edtTextPhoneNumber.setEnabled(false);
        edtTextEmail.setEnabled(false);
        edtTextSalary.setEnabled(false);
        edtTextPanCardNo.setEnabled(false);
    }

    @Override
    public void onSaveUnsuccess(String message) {
        SnackNotify.showMessage(message, frameLayout);
    }

    @Override
    public void onSaveInternetError() {

        SnackNotify.checkConnection(onretrySaveProfileApi, frameLayout);
    }

    OnClickInterface onretrySaveProfileApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getUpdateProfileDataApi();
        }
    };


    private void showAlertDialog(String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(true);

        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ((NavigationActivity)getActivity()).openDashboard();
                dialog.dismiss();
            }
        });

        alertDialog.show();
    }
    /*-----------------------------on click method------------------------------------*/

    @OnClick(R.id.imgViewFirstName)
    public void imgViewFirstNameClicked() {
        Log.e("imgViewFirstName", "Clicked");
        edtTextFirstName.setEnabled(true);
        edtTextFirstName.setAlpha(1);
        edtTextFirstName.setCursorVisible(true);
        edtTextFirstName.setSelection(edtTextFirstName.getText().toString().length());
    }

    @OnClick(R.id.imgViewLastName)
    public void imgViewLastNameClicked() {
        edtTextLastName.setEnabled(true);
        edtTextLastName.setAlpha(1);
        edtTextLastName.setCursorVisible(true);
        edtTextLastName.setSelection(edtTextLastName.getText().toString().length());
    }

    @OnClick(R.id.imgViewDOB)
    public void imgViewDOBClicked() {
        Log.e("imgViewDOB", "Clicked");
        txtViewDob.setEnabled(true);
        txtViewDob.setAlpha(1);

    }

    @OnClick(R.id.imgViewAddress)
    public void imgViewAddressClicked() {
        edtTextAddress.setEnabled(true);
        edtTextAddress.setAlpha(1);
        edtTextAddress.setCursorVisible(true);
        edtTextAddress.setSelection(edtTextAddress.getText().toString().length());
    }

    @OnClick(R.id.imgViewPhoneNumber)
    public void imgViewPhoneNumberClicked() {
        edtTextPhoneNumber.setEnabled(true);
        edtTextPhoneNumber.setAlpha(1);
        edtTextPhoneNumber.setCursorVisible(true);
        edtTextPhoneNumber.setSelection(edtTextPhoneNumber.getText().toString().length());
    }

    @OnClick(R.id.imgViewEmail)
    public void imgViewEmailClicked() {
        edtTextEmail.setEnabled(true);
        edtTextEmail.setAlpha(1);
        edtTextEmail.setCursorVisible(true);
        edtTextEmail.setSelection(edtTextEmail.getText().toString().length());
    }

    @OnClick(R.id.imgViewSalary)
    public void imgViewSalaryClicked() {
        edtTextSalary.setEnabled(true);
        edtTextSalary.setAlpha(1);
        edtTextSalary.setCursorVisible(true);
        edtTextSalary.setSelection(edtTextSalary.getText().toString().length());
    }

    @OnClick(R.id.imgViewPanCard)
    public void imgViewPanCardClicked() {
        edtTextPanCardNo.setEnabled(true);
        edtTextPanCardNo.setAlpha(1);
        edtTextPanCardNo.setCursorVisible(true);
        edtTextPanCardNo.setSelection(edtTextPanCardNo.getText().toString().length());
    }

    @OnClick(R.id.btnSubmit)
    public void btnSubmitClicked() {

        // hide keyboard
        Utils.hideKeyboardIfOpen(getActivity());
        getUpdateProfileDataApi();
    }

    @OnClick(R.id.txtViewDob)
    public void setEdtTextDOBClicked() {

        if (txtViewDob.isEnabled()) {
            DialogFragment dialogFragment = new DateFragment();
            //dialogFragment.show(getActivity().getSupportFragmentManager(), "DatePcker");
            dialogFragment.show(getActivity().getSupportFragmentManager(), "profile_fragment");
        }
    }


      /*--------------------------------public method-------------------------------------------*/

    // setting populated date
    public void populateSetDate(int year, int month, int day) {
        showDate(year, month, day);
    }

    private void showDate(int year, int month, int day) {
        String date = new StringBuilder().append(day).append("-").append(month).append("-").append(year).toString();

        txtViewDob.setText(date);
    }

    private void getUpdateProfileDataApi() {

        String firstName = String.valueOf(edtTextFirstName.getText());
        String lastName = String.valueOf(edtTextLastName.getText());
        String dob = String.valueOf(txtViewDob.getText());
        String address = String.valueOf(edtTextAddress.getText());
        String phoneNumber = String.valueOf(edtTextPhoneNumber.getText());
        String email = String.valueOf(edtTextEmail.getText());
        String salary = String.valueOf(edtTextSalary.getText());
        String panCardNo = String.valueOf(edtTextPanCardNo.getText());

        profilePresenter.updateProfile(firstName, lastName, dob, address, phoneNumber, email, salary, panCardNo);

    }

    private void getProfileDataApi() {
        profilePresenter.getProfile();
    }

    private void setField() {



        edtTextFirstName.setText(String.valueOf(arrayListProfile.get(0).getFirstName()));
        edtTextLastName.setText(String.valueOf(arrayListProfile.get(0).getLastName()));
        txtViewDob.setText(DateFormat.dd_mm_yyyy(String.valueOf(arrayListProfile.get(0).getDob())));
        edtTextAddress.setText(String.valueOf(arrayListProfile.get(0).getAddress()));
        edtTextPhoneNumber.setText(String.valueOf(arrayListProfile.get(0).getMobile()));
        edtTextEmail.setText(String.valueOf(arrayListProfile.get(0).getEmail()));
        edtTextSalary.setText(String.valueOf(arrayListProfile.get(0).getSalary()));
        edtTextPanCardNo.setText(String.valueOf(arrayListProfile.get(0).getPanCardNo()));

    }
}
