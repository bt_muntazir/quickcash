package com.quickcash.mvp.profile.presenter;

import android.app.Activity;
import android.provider.Settings;

import com.quickcash.BuildConfig;
import com.quickcash.R;
import com.quickcash.common.helpers.LoginManager;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ApiAdapter;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.requestresponse.ConstSession;
import com.quickcash.common.session.FcmSession;
import com.quickcash.common.utility.PrefUtil;
import com.quickcash.mvp.common_mvp.CommonModel;
import com.quickcash.mvp.login.model.LoginResponseModel;
import com.quickcash.mvp.login.presenter.LoginContractor;
import com.quickcash.mvp.profile.ProfileModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 15-01-2018.
 */

public class ProfilePresenterImpl implements ProfileContractor.Presenter {

    ProfileContractor.ProfileView profileView;
    Activity activity;
    JSONObject jsonObject;

    public ProfilePresenterImpl(ProfileContractor.ProfileView profileView, Activity activity) {

        this.profileView = profileView;
        this.activity = activity;
    }

    @Override
    public void getProfile() {
        try {
            ApiAdapter.getInstance(activity);
            getProfileData();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            profileView.onInternetError();
        }
    }

    @Override
    public void updateProfile(String firstName, String lastName, String dob, String address, String phoneNumber, String email, String salary, String panCardNo) {
        try {
            ApiAdapter.getInstance(activity);
            getUpdateProfileData(firstName, lastName, dob, address, phoneNumber, email, salary, panCardNo);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            profileView.onInternetError();
        }
    }


    private void getProfileData() {
        Progress.start(activity);

        int userId = LoginManager.getInstance().getUserData().getUserId();

        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_ID, userId);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE, Const.PARAM_ANDROID);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }


        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<ProfileModel> getOutput = ApiAdapter.getApiService().userProfile("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    ProfileModel profileModel = response.body();

                    if (profileModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        profileView.getProfileSuccess(profileModel.getData());
                    } else {
                        profileView.apiUnsuccess(profileModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    profileView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                Progress.stop();
                t.printStackTrace();
                profileView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }

    private void getUpdateProfileData(String firstName, String lastName, String dob, String address, String phoneNumber, String email, String salary, String panCardNo) {

        Progress.start(activity);

        int userId = LoginManager.getInstance().getUserData().getUserId();

        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();
        String currentLatitude = PrefUtil.getString(activity, ConstSession.PREF_LATITUIDE, "");
        String currentLongitude = PrefUtil.getString(activity, ConstSession.PREF_LONGITUDE, "");


        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_ID, userId);
            jsonObject.put(Const.PARAM_FIRST_NAME, firstName);
            jsonObject.put(Const.PARAM_LAST_NAME, lastName);
            jsonObject.put(Const.PARAM_EMAIL, email);
            jsonObject.put(Const.PARAM_MOBILE, phoneNumber);
            jsonObject.put(Const.PARAM_GENDER, "Male");
            jsonObject.put(Const.PARAM_ADDRESS, address);
            jsonObject.put(Const.PARAM_CURRENT_LONG, currentLongitude);
            jsonObject.put(Const.PARAM_CURRENT_LAT, currentLatitude);
            jsonObject.put(Const.PARAM_PIN_CODE, "155146");
            jsonObject.put(Const.PARAM_DOB, dob);
            jsonObject.put(Const.PARAM_PAN_CARD_NO, panCardNo);
            jsonObject.put(Const.PARAM_SALARY, salary);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE, Const.PARAM_ANDROID);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }


        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getOutput = ApiAdapter.getApiService().updateProfile("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                Progress.stop();
                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();
                    if (commonModel.getStatus() == (Const.PARAM_SUCCESS)) {
                        profileView.onSaveSuccess(commonModel.getMessage());
                    } else {
                        profileView.apiUnsuccess(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    profileView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                profileView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
