package com.quickcash.mvp.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Braintech on 15-01-2018.
 */

public class ProfileModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("photo")
        @Expose
        private String photo;
        @SerializedName("pan_card")
        @Expose
        private String panCard;
        @SerializedName("bank_statement")
        @Expose
        private String bankStatement;
        @SerializedName("address_proof")
        @Expose
        private String addressProof;
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("salary")
        @Expose
        private Integer salary;
        @SerializedName("pan_card_no")
        @Expose
        private String panCardNo;
        @SerializedName("latitute")
        @Expose
        private Object latitute;
        @SerializedName("longitute")
        @Expose
        private Object longitute;

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getPanCard() {
            return panCard;
        }

        public void setPanCard(String panCard) {
            this.panCard = panCard;
        }

        public String getBankStatement() {
            return bankStatement;
        }

        public void setBankStatement(String bankStatement) {
            this.bankStatement = bankStatement;
        }

        public String getAddressProof() {
            return addressProof;
        }

        public void setAddressProof(String addressProof) {
            this.addressProof = addressProof;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Integer getSalary() {
            return salary;
        }

        public void setSalary(Integer salary) {
            this.salary = salary;
        }

        public String getPanCardNo() {
            return panCardNo;
        }

        public void setPanCardNo(String panCardNo) {
            this.panCardNo = panCardNo;
        }

        public Object getLatitute() {
            return latitute;
        }

        public void setLatitute(Object latitute) {
            this.latitute = latitute;
        }

        public Object getLongitute() {
            return longitute;
        }

        public void setLongitute(Object longitute) {
            this.longitute = longitute;
        }
    }
}