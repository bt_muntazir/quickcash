package com.quickcash.mvp.profile.presenter;

import com.quickcash.mvp.common_mvp.BaseView;
import com.quickcash.mvp.login.model.LoginResponseModel;
import com.quickcash.mvp.mycash_application.MyCashApplicationModel;
import com.quickcash.mvp.profile.ProfileModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 15-01-2018.
 */

public class ProfileContractor {
    public interface ProfileView extends BaseView {

        void getProfileSuccess(ArrayList<ProfileModel.Datum> profileModel);

        void onSaveSuccess(String message);

        void onSaveUnsuccess(String message);

        void onSaveInternetError();
    }

    public interface Presenter {
        void getProfile();

        void updateProfile(String firstName, String lastName, String dob, String address, String phoneNumber, String email, String salary, String panCardNo);

    }
}
