package com.quickcash.mvp.applied_loan_detail.presenter;

import android.app.Activity;
import android.provider.Settings;

import com.quickcash.BuildConfig;
import com.quickcash.R;
import com.quickcash.common.helpers.LoginManager;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ApiAdapter;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.session.FcmSession;
import com.quickcash.mvp.applied_loan_detail.AppliedLoanDetailModel;
import com.quickcash.mvp.loan_detail.LoanDetailModel;
import com.quickcash.mvp.loan_detail.presenter.LoanDetailContractor;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 19-01-2018.
 */

public class AppliedLoanDetailPresenterImpl implements AppliedLoanDetailContractor.Presenter{

    AppliedLoanDetailContractor.AppliedLoanDetailView appliedLoanDetailView;
    Activity activity;
    JSONObject jsonObject;

    public AppliedLoanDetailPresenterImpl(AppliedLoanDetailContractor.AppliedLoanDetailView appliedLoanDetailView, Activity activity) {
        this.appliedLoanDetailView = appliedLoanDetailView;
        this.activity = activity;
    }

    @Override
    public void getAppliedLoanDetail(int loanId) {

        try {
            ApiAdapter.getInstance(activity);
            getAppliedLoanDetailData(loanId);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            appliedLoanDetailView.onInternetError();
        }
    }


    private void getAppliedLoanDetailData(int loanId) {
        Progress.start(activity);

        int userId = LoginManager.getInstance().getUserData().getUserId();
        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_LOAN_ID, loanId);
            jsonObject.put(Const.PARAM_USER_ID, userId);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE, Const.PARAM_ANDROID);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<AppliedLoanDetailModel> getOutput = ApiAdapter.getApiService().appliedLoanDetail("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<AppliedLoanDetailModel>() {
            @Override
            public void onResponse(Call<AppliedLoanDetailModel> call, Response<AppliedLoanDetailModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    AppliedLoanDetailModel loanDetailModel = response.body();

                    if (loanDetailModel.getStatus() == (Const.PARAM_SUCCESS)) {
                        appliedLoanDetailView.getAppliedLoanDetailSuccess(loanDetailModel.getData());
                    } else {
                        appliedLoanDetailView.apiUnsuccess(loanDetailModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    appliedLoanDetailView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<AppliedLoanDetailModel> call, Throwable t) {
                t.printStackTrace();
                Progress.stop();
                appliedLoanDetailView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
