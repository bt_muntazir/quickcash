package com.quickcash.mvp.applied_loan_detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Braintech on 19-01-2018.
 */

public class AppliedLoanDetailModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("tenure_interest_id")
        @Expose
        private Integer tenureInterestId;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("loan_amount")
        @Expose
        private String loanAmount;
        @SerializedName("tenure")
        @Expose
        private String tenure;
        @SerializedName("interest_rate")
        @Expose
        private String interestRate;
        @SerializedName("processing_fees")
        @Expose
        private String processingFees;
        @SerializedName("interest_amount")
        @Expose
        private String interestAmount;
        @SerializedName("total_amount_to_paid")
        @Expose
        private String totalAmountToPaid;
        @SerializedName("applied_date")
        @Expose
        private String appliedDate;
        @SerializedName("active_date")
        @Expose
        private String activeDate;
        @SerializedName("due_date")
        @Expose
        private String dueDate;
        @SerializedName("approved_status")
        @Expose
        private String approvedStatus;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("payment_mode")
        @Expose
        private String paymentMode;

        public Integer getTenureInterestId() {
            return tenureInterestId;
        }

        public void setTenureInterestId(Integer tenureInterestId) {
            this.tenureInterestId = tenureInterestId;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getLoanAmount() {
            return loanAmount;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public void setLoanAmount(String loanAmount) {
            this.loanAmount = loanAmount;
        }

        public String getTenure() {
            return tenure;
        }

        public void setTenure(String tenure) {
            this.tenure = tenure;
        }

        public String getInterestRate() {
            return interestRate;
        }

        public void setInterestRate(String interestRate) {
            this.interestRate = interestRate;
        }

        public String getProcessingFees() {
            return processingFees;
        }

        public void setProcessingFees(String processingFees) {
            this.processingFees = processingFees;
        }

        public String getInterestAmount() {
            return interestAmount;
        }

        public void setInterestAmount(String interestAmount) {
            this.interestAmount = interestAmount;
        }

        public String getTotalAmountToPaid() {
            return totalAmountToPaid;
        }

        public void setTotalAmountToPaid(String totalAmountToPaid) {
            this.totalAmountToPaid = totalAmountToPaid;
        }

        public String getAppliedDate() {
            return appliedDate;
        }

        public void setAppliedDate(String appliedDate) {
            this.appliedDate = appliedDate;
        }

        public String getActiveDate() {
            return activeDate;
        }

        public void setActiveDate(String activeDate) {
            this.activeDate = activeDate;
        }

        public String getDueDate() {
            return dueDate;
        }

        public void setDueDate(String dueDate) {
            this.dueDate = dueDate;
        }

        public String getApprovedStatus() {
            return approvedStatus;
        }

        public void setApprovedStatus(String approvedStatus) {
            this.approvedStatus = approvedStatus;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }
    }
}