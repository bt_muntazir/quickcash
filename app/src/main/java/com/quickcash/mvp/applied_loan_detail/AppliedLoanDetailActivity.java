package com.quickcash.mvp.applied_loan_detail;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quickcash.R;
import com.quickcash.common.helpers.FontHelper;
import com.quickcash.common.interfaces.OnClickInterface;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.requestresponse.ConstIntent;
import com.quickcash.common.utility.SnackNotify;
import com.quickcash.common.utility.Utils;
import com.quickcash.mvp.applied_loan_detail.presenter.AppliedLoanDetailContractor;
import com.quickcash.mvp.applied_loan_detail.presenter.AppliedLoanDetailPresenterImpl;
import com.quickcash.mvp.disbursement.DisbursementActivity;
import com.quickcash.mvp.loan_packages.LoanPackagesActivity;
import com.quickcash.mvp.payment.PaymentActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AppliedLoanDetailActivity extends AppCompatActivity implements AppliedLoanDetailContractor.AppliedLoanDetailView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.txtViewCurrentApplication)
    TextView txtViewCurrentApplication;


    @BindView(R.id.txtViewLoanDetailStatus)
    TextView txtViewLoanDetailStatus;


    @BindView(R.id.relLayLoanAmount)
    RelativeLayout relLayLoanAmount;

    @BindView(R.id.txtViewLoanAmountTitle)
    TextView txtViewLoanAmountTitle;

    @BindView(R.id.txtViewLoanAmount)
    TextView txtViewLoanAmount;


    @BindView(R.id.relLayProcessingFee)
    RelativeLayout relLayProcessingFee;

    @BindView(R.id.txtViewProcessingFeeTitle)
    TextView txtViewProcessingFeeTitle;

    @BindView(R.id.txtViewProcessingFee)
    TextView txtViewProcessingFee;


    @BindView(R.id.relLayInterestAmount)
    RelativeLayout relLayInterestAmount;

    @BindView(R.id.txtViewInterestAmountTitle)
    TextView txtViewInterestAmountTitle;

    @BindView(R.id.txtViewInterestAmount)
    TextView txtViewInterestAmount;


    @BindView(R.id.relLayTotalpayment)
    RelativeLayout relLayTotalpayment;

    @BindView(R.id.txtViewTotalPaymentTitle)
    TextView txtViewTotalPaymentTitle;

    @BindView(R.id.txtViewTotalPayment)
    TextView txtViewTotalPayment;


    @BindView(R.id.relLayAppliedDate)
    RelativeLayout relLayAppliedDate;

    @BindView(R.id.txtViewAppliedDateTitle)
    TextView txtViewAppliedDateTitle;

    @BindView(R.id.txtViewAppliedDate)
    TextView txtViewAppliedDate;


    @BindView(R.id.relLayActiveDate)
    RelativeLayout relLayActiveDate;

    @BindView(R.id.txtViewActiveDateTitle)
    TextView txtViewActiveDateTitle;

    @BindView(R.id.txtViewActiveDate)
    TextView txtViewActiveDate;


    @BindView(R.id.relLayDueDate)
    RelativeLayout relLayDueDate;

    @BindView(R.id.txtViewDueDateTitle)
    TextView txtViewDueDateTitle;

    @BindView(R.id.txtViewDueDate)
    TextView txtViewDueDate;

    @BindView(R.id.relLayPayMode)
    RelativeLayout relLayPayMode;

    @BindView(R.id.txtViewPayModeTitle)
    TextView txtViewPayModeTitle;

    @BindView(R.id.txtViewPayMode)
    TextView txtViewPayMode;


    @BindView(R.id.txtViewMessage)
    TextView txtViewMessage;

    @BindView(R.id.btnSubmit)
    TextView btnSubmit;

    @BindView(R.id.txtViewPoweredBy)
    TextView txtViewPoweredBy;

    int loanId;
    AppliedLoanDetailPresenterImpl appliedLoanDetailPresenter;
    AppliedLoanDetailModel.Data appliedLoanDetailData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_applied_loan_detail);
        ButterKnife.bind(this);
        appliedLoanDetailPresenter = new AppliedLoanDetailPresenterImpl(this, this);
        setFont();
        getIntentData();
        getUserAppliedLoanDetailDataApi();
    }

    private void getUserAppliedLoanDetailDataApi() {
        appliedLoanDetailPresenter.getAppliedLoanDetail(loanId);
    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            loanId = getIntent().getIntExtra(ConstIntent.KEY_LOAN_ID, -1);
        }
    }

    private void setFont(){
        FontHelper.applyFont(this, txtViewLoanDetailStatus, FontHelper.FontType.FONT_BOLD);


        FontHelper.applyFont(this, txtViewLoanAmountTitle, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewProcessingFeeTitle, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewInterestAmountTitle, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewTotalPaymentTitle, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewAppliedDateTitle, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewActiveDateTitle, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewDueDateTitle, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewPayModeTitle, FontHelper.FontType.FONT_REGULAR);


        FontHelper.applyFont(this, txtViewLoanAmount, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewProcessingFee, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewInterestAmount, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewTotalPayment, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewAppliedDate, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewActiveDate, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewDueDate, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewPayMode, FontHelper.FontType.FONT_BOLD);

        FontHelper.applyFont(this, txtViewMessage, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, btnSubmit, FontHelper.FontType.FONT_BOLD);
    }


    @OnClick(R.id.imgViewBack)
    public void imgViewBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void onInternetError() {
        SnackNotify.checkConnection(onRetryGetUserAppliedLoan, relLayMain);
    }

    OnClickInterface onRetryGetUserAppliedLoan = new OnClickInterface() {
        @Override
        public void onClick() {
            getUserAppliedLoanDetailDataApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void getAppliedLoanDetailSuccess(AppliedLoanDetailModel.Data appliedLoanDetailData) {
        this.appliedLoanDetailData = appliedLoanDetailData;

        setField();
    }

    private void setField() {

        if (appliedLoanDetailData.getApprovedStatus().equals(Const.KEY_NEW)) {
            txtViewLoanDetailStatus.setText("NEW LOAN");
            btnSubmit.setVisibility(View.GONE);
        } else if (appliedLoanDetailData.getApprovedStatus().equals(Const.KEY_REJECTED)) {
            txtViewLoanDetailStatus.setText("LOAN REJECTED");
        } else if (appliedLoanDetailData.getApprovedStatus().equals(Const.KEY_APPROVED)) {

            if (appliedLoanDetailData.getStatus().equals(Const.KEY_ACTIVE)) {
                txtViewLoanDetailStatus.setText("MY ACTIVE LOAN");
            } else if (appliedLoanDetailData.getStatus().equals(Const.KEY_INACTIVE)) {
                txtViewLoanDetailStatus.setText("CONDITIONALLY APPROVED");
            } else if (appliedLoanDetailData.getStatus().equals(Const.KEY_PAID)) {
                txtViewLoanDetailStatus.setText("CLOSED");
                txtViewTotalPaymentTitle.setText("You Paid");
            }
        }

        if (!Utils.isEmptyOrNull(appliedLoanDetailData.getPaymentMode())) {

            if (appliedLoanDetailData.getApprovedStatus().equals(Const.KEY_REJECTED)) {
                btnSubmit.setText("APPLY NEW LOAN");
            } else if (appliedLoanDetailData.getApprovedStatus().equals(Const.KEY_APPROVED)) {

                if (appliedLoanDetailData.getStatus().equals(Const.KEY_ACTIVE)) {
                    btnSubmit.setText("PAY LOAN");
                    txtViewMessage.setVisibility(View.GONE);
                } else if (appliedLoanDetailData.getStatus().equals(Const.KEY_INACTIVE)) {
                    btnSubmit.setText("APPLY NEW LOAN");
                } else if (appliedLoanDetailData.getStatus().equals(Const.KEY_PAID)) {
                    txtViewLoanDetailStatus.setText("CLOSED");
                    btnSubmit.setText("APPLY NEW LOAN");
                }
            }
        } else {
            btnSubmit.setText("CHOOSE DISBURSEMENT TYPE");
        }

        if (!Utils.isEmptyOrNull(appliedLoanDetailData.getLoanAmount())) {
            relLayLoanAmount.setVisibility(View.VISIBLE);
            txtViewLoanAmount.setText(appliedLoanDetailData.getLoanAmount());
        } else {
            relLayLoanAmount.setVisibility(View.GONE);
        }

        if (!Utils.isEmptyOrNull(appliedLoanDetailData.getProcessingFees())) {
            relLayProcessingFee.setVisibility(View.VISIBLE);
            txtViewProcessingFee.setText(appliedLoanDetailData.getProcessingFees());
        } else {
            relLayProcessingFee.setVisibility(View.GONE);
        }

        if (!Utils.isEmptyOrNull(appliedLoanDetailData.getInterestAmount())) {
            relLayInterestAmount.setVisibility(View.VISIBLE);
            txtViewInterestAmount.setText(appliedLoanDetailData.getInterestAmount());
        } else {
            relLayInterestAmount.setVisibility(View.GONE);
        }

        if (!Utils.isEmptyOrNull(appliedLoanDetailData.getTotalAmountToPaid())) {
            relLayTotalpayment.setVisibility(View.VISIBLE);
            txtViewTotalPayment.setText(appliedLoanDetailData.getTotalAmountToPaid());
        } else {
            relLayTotalpayment.setVisibility(View.GONE);
        }

        if (!Utils.isEmptyOrNull(appliedLoanDetailData.getAppliedDate())) {
            relLayAppliedDate.setVisibility(View.VISIBLE);

            txtViewAppliedDate.setText(appliedLoanDetailData.getAppliedDate());
        } else {
            relLayAppliedDate.setVisibility(View.GONE);
        }

        if (!Utils.isEmptyOrNull(appliedLoanDetailData.getActiveDate())) {
            relLayActiveDate.setVisibility(View.VISIBLE);
            txtViewActiveDate.setText(appliedLoanDetailData.getActiveDate());
        } else {
            relLayActiveDate.setVisibility(View.GONE);
        }

        if (!Utils.isEmptyOrNull(appliedLoanDetailData.getDueDate())) {
            relLayDueDate.setVisibility(View.VISIBLE);
            txtViewDueDate.setText(appliedLoanDetailData.getDueDate());
        } else {
            relLayDueDate.setVisibility(View.GONE);
        }

        if (!Utils.isEmptyOrNull(appliedLoanDetailData.getMessage())) {
            txtViewMessage.setVisibility(View.VISIBLE);
            if (appliedLoanDetailData.getStatus().equals(Const.KEY_PAID)) {
                txtViewMessage.setText("Thank you for your loan repayment.Apply for a new loan now.");
            } else {
                txtViewMessage.setText(appliedLoanDetailData.getMessage());
            }
        } else {
            txtViewMessage.setVisibility(View.GONE);
        }

        if (!Utils.isEmptyOrNull(appliedLoanDetailData.getPaymentMode())) {
            relLayPayMode.setVisibility(View.VISIBLE);
            txtViewPayMode.setText(appliedLoanDetailData.getPaymentMode());
        } else {
            relLayPayMode.setVisibility(View.GONE);
        }

    }

    @OnClick(R.id.btnSubmit)
    public void btnSubmiyClicked() {

        if (!Utils.isEmptyOrNull(appliedLoanDetailData.getPaymentMode())) {

            if (appliedLoanDetailData.getApprovedStatus().equals(Const.KEY_NEW)) {

            } else if (appliedLoanDetailData.getApprovedStatus().equals(Const.KEY_REJECTED)) {
                Intent intent = new Intent(this, LoanPackagesActivity.class);
                startActivity(intent);
            } else if (appliedLoanDetailData.getApprovedStatus().equals(Const.KEY_APPROVED)) {

                if (appliedLoanDetailData.getStatus().equals(Const.KEY_ACTIVE)) {
                    Intent intent = new Intent(this, PaymentActivity.class);
                    startActivity(intent);
                } else if (appliedLoanDetailData.getStatus().equals(Const.KEY_INACTIVE)) {
                    Intent intent = new Intent(this, LoanPackagesActivity.class);
                    startActivity(intent);

                } else if (appliedLoanDetailData.getStatus().equals(Const.KEY_PAID)) {
                    Intent intent = new Intent(this, LoanPackagesActivity.class);
                    startActivity(intent);
                }
            }

        } else {
            Intent intent = new Intent(this, DisbursementActivity.class);
            intent.putExtra(ConstIntent.KEY_LOAN_ID, loanId);
            startActivity(intent);
        }


    }
}

