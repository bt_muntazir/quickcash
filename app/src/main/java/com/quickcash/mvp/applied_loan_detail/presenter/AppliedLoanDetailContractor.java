package com.quickcash.mvp.applied_loan_detail.presenter;

import com.quickcash.mvp.applied_loan_detail.AppliedLoanDetailModel;
import com.quickcash.mvp.common_mvp.BaseView;
import com.quickcash.mvp.loan_detail.LoanDetailModel;

/**
 * Created by Braintech on 19-01-2018.
 */

public class AppliedLoanDetailContractor {
    public interface AppliedLoanDetailView extends BaseView {

        void getAppliedLoanDetailSuccess(AppliedLoanDetailModel.Data appliedLoanDetailData);
    }

    public interface Presenter {
        void getAppliedLoanDetail(int loanId);
    }
}
