package com.quickcash.mvp.my_loans;

import android.app.Activity;
import android.provider.Settings;

import com.google.gson.JsonParser;
import com.quickcash.BuildConfig;
import com.quickcash.R;
import com.quickcash.common.helpers.LoginManager;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ApiAdapter;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.session.FcmSession;
import com.quickcash.mvp.mycash_application.MyCashApplicationModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by BRAINTECH on 24-01-2018.
 */

public class MyLoansPresenterImpl implements MyLoansPresenter {


    Activity activity;
    MyLoansView myLoansView;
    JSONObject jsonObject;

    public MyLoansPresenterImpl(Activity activity,MyLoansView myLoansView){
        this.activity=activity;
        this.myLoansView=myLoansView;
    }

    @Override
    public void callingMyLoanApi() {
        try {
            ApiAdapter.getInstance(activity);
            gettingResult();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            myLoansView.onInternetError();
        }
    }

    private void gettingResult(){
        Progress.start(activity);

        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();
        int userId= LoginManager.getInstance().getUserData().getUserId();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_ID,userId);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE, "A");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<MyCashApplicationModel> getOutput = ApiAdapter.getApiService().appliedLoans("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<MyCashApplicationModel>() {
            @Override
            public void onResponse(Call<MyCashApplicationModel> call, Response<MyCashApplicationModel> response) {
                Progress.stop();

                try {
                    //getting whole data from response
                    MyCashApplicationModel myCashApplicationModel = response.body();
                    if (myCashApplicationModel.getStatus()==(Const.PARAM_SUCCESS)) {
                        myLoansView.onSuccess(myCashApplicationModel.getData());
                    } else {
                        myLoansView.onUnsuccess(myCashApplicationModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    myLoansView.onUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<MyCashApplicationModel> call, Throwable t) {
                Progress.stop();
                myLoansView.onUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
