package com.quickcash.mvp.my_loans;

import com.quickcash.mvp.mycash_application.MyCashApplicationModel;

import java.util.ArrayList;

/**
 * Created by BRAINTECH on 24-01-2018.
 */

public interface MyLoansView {
    public void onSuccess(ArrayList<MyCashApplicationModel.Datum> data);
    public void onUnsuccess(String message);
    public void onInternetError();
}
