package com.quickcash.mvp.my_loans;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quickcash.R;
import com.quickcash.common.interfaces.OnClickInterface;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.utility.SnackNotify;
import com.quickcash.mvp.mycash_application.MyCashApplicationAdapter;
import com.quickcash.mvp.mycash_application.MyCashApplicationModel;
import com.quickcash.mvp.mycash_application.MyCashApplicationPresenterImpl;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyLoansActivity extends AppCompatActivity implements MyLoansView{

    @BindView(R.id.relRoot)
    RelativeLayout relRoot;

    @BindView(R.id.txtViewAppliedLoans)
    TextView txtViewAppliedLoans;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.imgViewNotification)
    ImageView imgViewNotification;

    MyLoansPresenterImpl myLoansPresenterImpl;
    ArrayList<MyCashApplicationModel.Datum> datumArrayList;
    ArrayList<MyCashApplicationModel.Datum> finalDatumArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_loans);
        ButterKnife.bind(this);
        callingMyCashApplication();
        finalDatumArrayList=new ArrayList<>();
    }

    private void callingMyCashApplication() {
        myLoansPresenterImpl = new MyLoansPresenterImpl(this, this);
        myLoansPresenterImpl.callingMyLoanApi();
    }

    @Override
    public void onSuccess(ArrayList<MyCashApplicationModel.Datum> datumArrayList) {
        this.datumArrayList = datumArrayList;

        for (int i = 0; i < datumArrayList.size(); i++) {
            if ((datumArrayList.get(i).getApprovedStatus().equals(Const.KEY_APPROVED))) {

                finalDatumArrayList.add(datumArrayList.get(i));
            }
        }
        setLayoutManager();
    }

    @Override
    public void onUnsuccess(String message) {
        SnackNotify.showMessage(message, relRoot);
    }

    @Override
    public void onInternetError() {
        SnackNotify.checkConnection(onRetry, relRoot);
    }

    OnClickInterface onRetry = new OnClickInterface() {
        @Override
        public void onClick() {
            callingMyCashApplication();
        }
    };

    private void setLayoutManager() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        setAdapter();
    }

    private void setAdapter() {
        MyCashApplicationAdapter myCashApplicationAdapter = new MyCashApplicationAdapter(this, finalDatumArrayList);
        recyclerView.setAdapter(myCashApplicationAdapter);
    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBack(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.imgViewNotification)
    public void imgViewNotification(){

    }
}
