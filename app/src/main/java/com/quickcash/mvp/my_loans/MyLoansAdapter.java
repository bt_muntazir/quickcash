package com.quickcash.mvp.my_loans;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quickcash.R;
import com.quickcash.common.helpers.FontHelper;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.requestresponse.ConstIntent;
import com.quickcash.mvp.applied_loan_detail.AppliedLoanDetailActivity;
import com.quickcash.mvp.mycash_application.MyCashApplicationAdapter;
import com.quickcash.mvp.mycash_application.MyCashApplicationModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by BRAINTECH on 24-01-2018.
 */

public class MyLoansAdapter extends RecyclerView.Adapter<MyLoansAdapter.Holder> {

    Activity activity;
    ArrayList<MyCashApplicationModel.Datum> datumArrayList;

    public MyLoansAdapter(Activity activity, ArrayList<MyCashApplicationModel.Datum> datumArrayList) {
        this.activity = activity;
        this.datumArrayList = datumArrayList;
    }

    @Override
    public MyLoansAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_mycash_application, parent, false);
        return new MyLoansAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(MyLoansAdapter.Holder holder, int position) {
        final MyCashApplicationModel.Datum datum = datumArrayList.get(position);


            holder.txtViewTime.setText(datum.getTenure());
            holder.txtViewAmount.setText(Const.KEY_PRICE_SYMBOL + " " + datum.getLoanAmount());
            holder.txtViewInterest.setText(datum.getInterestRate() + "%");
            holder.txtViewProcessingFees.setText(Const.KEY_PRICE_SYMBOL + " " + datum.getProcessingFees());
            holder.txtViewDate.setText(datum.getAppliedDate());
            holder.txtViewPackageName.setText(datum.getPackage_name());


            holder.txtViewViewLoanDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int user_loanId = datum.getId();
                    Intent intent = new Intent(activity, AppliedLoanDetailActivity.class);
                    intent.putExtra(ConstIntent.KEY_LOAN_ID, user_loanId);
                    activity.startActivity(intent);
                }
            });
    }

    @Override
    public int getItemCount() {
        return datumArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewTime)
        TextView txtViewTime;

        @BindView(R.id.txtViewTitleTime)
        TextView txtViewTitleTime;

        @BindView(R.id.txtViewAmount)
        TextView txtViewAmount;

        @BindView(R.id.txtViewTitleAmount)
        TextView txtViewTitleAmount;

        @BindView(R.id.txtViewInterest)
        TextView txtViewInterest;

        @BindView(R.id.txtViewTitleInterest)
        TextView txtViewTitleInterest;

        @BindView(R.id.txtViewTitleProcessingFees)
        TextView txtViewTitleProcessingFees;

        @BindView(R.id.txtViewProcessingFees)
        TextView txtViewProcessingFees;

        @BindView(R.id.txtViewTitleDate)
        TextView txtViewTitleDate;

        @BindView(R.id.txtViewDate)
        TextView txtViewDate;

        @BindView(R.id.txtViewViewLoanDetails)
        TextView txtViewViewLoanDetails;

        @BindView(R.id.txtViewTitlePackageName)
        TextView txtViewTitlePackageName;

        @BindView(R.id.txtViewPackageName)
        TextView txtViewPackageName;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            setFont();
        }

        private void setFont() {
            FontHelper.applyFont(activity, txtViewTime, FontHelper.FontType.FONT_BOLD);
            FontHelper.applyFont(activity, txtViewAmount, FontHelper.FontType.FONT_BOLD);
            FontHelper.applyFont(activity, txtViewInterest, FontHelper.FontType.FONT_BOLD);

            FontHelper.applyFont(activity, txtViewTitleTime, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewTitleAmount, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewTitleInterest, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewTitleProcessingFees, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewProcessingFees, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewTitlePackageName, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewPackageName, FontHelper.FontType.FONT_REGULAR);

            FontHelper.applyFont(activity, txtViewTitleDate, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewDate, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewViewLoanDetails, FontHelper.FontType.FONT_BOLD);


        }
    }
}
