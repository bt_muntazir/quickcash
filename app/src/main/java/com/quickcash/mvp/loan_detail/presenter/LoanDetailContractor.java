package com.quickcash.mvp.loan_detail.presenter;

import com.quickcash.mvp.common_mvp.BaseView;
import com.quickcash.mvp.loan_detail.LoanDetailModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 17-01-2018.
 */

public class LoanDetailContractor {

    public interface LoanDetailView extends BaseView {

        void getLoanDetailSuccess(LoanDetailModel.Data loanDetailModel);
    }

    public interface Presenter {
        void getLoanDetail(int loanId);
    }
}
