package com.quickcash.mvp.loan_detail;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.quickcash.R;
import com.quickcash.common.helpers.FontHelper;
import com.quickcash.common.requestresponse.ConstIntent;
import com.quickcash.common.utility.SeekbarWithIntervals;
import com.quickcash.mvp.loan_detail.presenter.LoanDetailContractor;
import com.quickcash.mvp.loan_detail.presenter.LoanDetailPresenterImpl;
import com.quickcash.mvp.upload_document.UploadDocumentActivity;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoanDetailActivity extends AppCompatActivity {

    @BindView(R.id.txtViewPickLoanAmount)
    TextView txtViewPickLoanAmount;

    @BindView(R.id.txtViewLoanSummary)
    TextView txtViewLoanSummary;

    @BindView(R.id.txtViewLoanAmountTitle)
    TextView txtViewLoanAmountTitle;

    @BindView(R.id.txtViewLoanAmount)
    TextView txtViewLoanAmount;

    @BindView(R.id.txtViewProcessingFeeTitle)
    TextView txtViewProcessingFeeTitle;

    @BindView(R.id.txtViewProcessingFee)
    TextView txtViewProcessingFee;

    @BindView(R.id.txtViewTotalInterestTitle)
    TextView txtViewTotalInterestTitle;

    @BindView(R.id.txtViewTotalInterest)
    TextView txtViewTotalInterest;

    @BindView(R.id.txtViewTotalRepaymentTitle)
    TextView txtViewTotalRepaymentTitle;

    @BindView(R.id.txtViewTotalRepayment)
    TextView txtViewTotalRepayment;

    @BindView(R.id.txtViewDueDateTitle)
    TextView txtViewDueDateTitle;

    @BindView(R.id.txtViewDueDate)
    TextView txtViewDueDate;

    @BindView(R.id.txtViewPoweredBy)
    TextView txtViewPoweredBy;

    @BindView(R.id.seekBarInterval)
    SeekbarWithIntervals seekBarInterval;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    LoanDetailModel.Data loanDetailModel;

    int loanId;
    int tenureInterestId;

    ArrayList<String> arrayListDays;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_detail);
        ButterKnife.bind(this);
        setFont();
        getIntentData();

        //setSeekBar();
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewPickLoanAmount, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewLoanSummary, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewLoanAmountTitle, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewLoanAmount, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewProcessingFeeTitle, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewProcessingFee, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewTotalInterestTitle, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewTotalInterest, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewTotalRepaymentTitle, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewTotalRepayment, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewDueDateTitle, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewDueDate, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewPoweredBy, FontHelper.FontType.FONT_REGULAR);

    }

    private void setField() {
        txtViewLoanAmount.setText(loanDetailModel.getLoanAmount());
        txtViewProcessingFee.setText(loanDetailModel.getProcessingFees());
        txtViewTotalInterest.setText(loanDetailModel.getInterest().get(0).getInterestAmount());
        txtViewTotalRepayment.setText(loanDetailModel.getInterest().get(0).getTotalAmount());
        //txtViewDueDate.setText(arrayListLoanDetail.get(0).get());
        tenureInterestId=loanDetailModel.getInterest().get(0).getTenureInterestId();
        Log.e("set field tenureIn",""+tenureInterestId);
    }

    @OnClick(R.id.btnSubmit)
    public void btnSubmitClicked() {

        Log.e("Submit tenureInterestId", "" + tenureInterestId);

        Intent intent = new Intent(this, UploadDocumentActivity.class);
        intent.putExtra(ConstIntent.KEY_LOAN_ID, loanId);
        intent.putExtra(ConstIntent.KEY_TENURE_INTEREST_ID, tenureInterestId);
        startActivity(intent);
    }

    private void getIntentData() {

        Bundle extras = getIntent().getExtras();

        if (extras != null) {

            loanDetailModel = (LoanDetailModel.Data) extras.getSerializable(ConstIntent.KEY_DATA);

            loanId = loanDetailModel.getId();

            setSeekBar();

            setField();
        }
    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setSeekBar() {

        final ArrayList<LoanDetailModel.Data.Interest> arrayListData = loanDetailModel.getInterest();

        String minTenture = loanDetailModel.getMinTenure();
        String maxTenture = loanDetailModel.getMaxTenure();


        arrayListDays = new ArrayList<String>();

        for (int i = 0; i < arrayListData.size(); i++) {

            LoanDetailModel.Data.Interest interest = arrayListData.get(i);

            String days = interest.getTenureTitle();
            arrayListDays.add(days);
        }

        seekBarInterval.setIntervals(arrayListDays, minTenture, maxTenture);


        seekBarInterval.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                String days = arrayListData.get(progress).getTenureTitle();

                seekBarInterval.updateTextView(progress, days);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                int progress = seekBar.getProgress();
                String days = arrayListData.get(progress).getTenureTitle();

                for (int i = 0; i < arrayListData.size(); i++) {

                    LoanDetailModel.Data.Interest interest = arrayListData.get(i);

                    if (interest.getTenureTitle().equals(days)) {

                        String interestAmount = interest.getInterestAmount();
                        String totalAmount = interest.getTotalAmount();
                        tenureInterestId = interest.getTenureInterestId();
                        int tenureId = interest.getTenureId();

                        Log.e("interestAmount", interestAmount);
                        Log.e("totalAmount", totalAmount);
                        Log.e("tenureInterestId", "" + tenureInterestId);
                        Log.e("tenureId", "" + tenureId);
                        txtViewLoanAmount.setText(loanDetailModel.getLoanAmount());
                        txtViewProcessingFee.setText(loanDetailModel.getProcessingFees());
                        txtViewTotalInterest.setText(interestAmount);
                        txtViewTotalRepayment.setText(totalAmount);

                    }
                }

            }

        });
    }


}
