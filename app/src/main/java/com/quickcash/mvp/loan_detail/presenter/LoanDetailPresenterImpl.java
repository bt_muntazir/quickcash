package com.quickcash.mvp.loan_detail.presenter;

import android.app.Activity;
import android.provider.Settings;

import com.quickcash.BuildConfig;
import com.quickcash.R;
import com.quickcash.common.helpers.LoginManager;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ApiAdapter;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.session.FcmSession;
import com.quickcash.mvp.common_mvp.CommonModel;
import com.quickcash.mvp.loan_detail.LoanDetailModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 17-01-2018.
 */

public class LoanDetailPresenterImpl implements LoanDetailContractor.Presenter {

    LoanDetailContractor.LoanDetailView loanDetailView;
    Activity activity;
    JSONObject jsonObject;

    public LoanDetailPresenterImpl(LoanDetailContractor.LoanDetailView loanDetailView, Activity activity) {
        this.loanDetailView = loanDetailView;
        this.activity = activity;
    }

    @Override
    public void getLoanDetail(int loanId) {

        try {
            ApiAdapter.getInstance(activity);
            getLoanDetailData(loanId);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            loanDetailView.onInternetError();
        }
    }


    private void getLoanDetailData(int loanId) {
        Progress.start(activity);

        int userId = LoginManager.getInstance().getUserData().getUserId();
        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_LOAN_ID, loanId);
            jsonObject.put(Const.PARAM_USER_ID, userId);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE,  Const.PARAM_ANDROID);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<LoanDetailModel> getOutput = ApiAdapter.getApiService().loanDetail("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<LoanDetailModel>() {
            @Override
            public void onResponse(Call<LoanDetailModel> call, Response<LoanDetailModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    LoanDetailModel loanDetailModel = response.body();

                    if (loanDetailModel.getStatus() == (Const.PARAM_SUCCESS)) {
                        loanDetailView.getLoanDetailSuccess(loanDetailModel.getData());
                    } else {
                        loanDetailView.apiUnsuccess(loanDetailModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    loanDetailView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<LoanDetailModel> call, Throwable t) {
                Progress.stop();
                loanDetailView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
