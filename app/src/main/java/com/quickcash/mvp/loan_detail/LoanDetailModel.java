package com.quickcash.mvp.loan_detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Braintech on 17-01-2018.
 */


public class LoanDetailModel implements Serializable {

    @SerializedName("status")
    @Expose
    private Integer status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data implements Serializable{

        @SerializedName("id")
        @Expose
        private Integer id;

        @SerializedName("loan_amount")
        @Expose
        private String loanAmount;

        @SerializedName("processing_fees")
        @Expose
        private String processingFees;

        @SerializedName("interest")
        @Expose
        private ArrayList<Interest> interest = null;

        @SerializedName("min_tenure")
        @Expose
        private String minTenure;

        @SerializedName("max_tenure")
        @Expose
        private String maxTenure;


        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getLoanAmount() {
            return loanAmount;
        }

        public void setLoanAmount(String loanAmount) {
            this.loanAmount = loanAmount;
        }

        public String getProcessingFees() {
            return processingFees;
        }

        public void setProcessingFees(String processingFees) {
            this.processingFees = processingFees;
        }

        public ArrayList<Interest> getInterest() {
            return interest;
        }

        public void setInterest(ArrayList<Interest> interest) {
            this.interest = interest;
        }

        public String getMinTenure() {
            return minTenure;
        }

        public void setMinTenure(String minTenure) {
            this.minTenure = minTenure;
        }

        public String getMaxTenure() {
            return maxTenure;
        }

        public void setMaxTenure(String maxTenure) {
            this.maxTenure = maxTenure;
        }

        public class Interest implements Serializable {

            @SerializedName("interest_rate")
            @Expose
            private String interestRate;
            @SerializedName("tenure_id")
            @Expose
            private Integer tenureId;
            @SerializedName("tenure_title")
            @Expose
            private String tenureTitle;
            @SerializedName("tenure_interest_id")
            @Expose
            private Integer tenureInterestId;
            @SerializedName("interest_amount")
            @Expose
            private String interestAmount;
            @SerializedName("total_amount")
            @Expose
            private String totalAmount;

            public String getInterestRate() {
                return interestRate;
            }

            public void setInterestRate(String interestRate) {
                this.interestRate = interestRate;
            }

            public Integer getTenureId() {
                return tenureId;
            }

            public void setTenureId(Integer tenureId) {
                this.tenureId = tenureId;
            }

            public String getTenureTitle() {
                return tenureTitle;
            }

            public void setTenureTitle(String tenureTitle) {
                this.tenureTitle = tenureTitle;
            }

            public Integer getTenureInterestId() {
                return tenureInterestId;
            }

            public void setTenureInterestId(Integer tenureInterestId) {
                this.tenureInterestId = tenureInterestId;
            }

            public String getInterestAmount() {
                return interestAmount;
            }

            public void setInterestAmount(String interestAmount) {
                this.interestAmount = interestAmount;
            }

            public String getTotalAmount() {
                return totalAmount;
            }

            public void setTotalAmount(String totalAmount) {
                this.totalAmount = totalAmount;
            }

        }
    }
}



