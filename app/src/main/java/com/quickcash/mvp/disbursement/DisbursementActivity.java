package com.quickcash.mvp.disbursement;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quickcash.R;
import com.quickcash.common.helpers.AlertDialogHelper;
import com.quickcash.common.helpers.FontHelper;
import com.quickcash.common.interfaces.OnClickInterface;
import com.quickcash.common.requestresponse.ConstIntent;
import com.quickcash.common.utility.SnackNotify;
import com.quickcash.mvp.disbursement.presenter.DisbursementContractor;
import com.quickcash.mvp.disbursement.presenter.DisbursementPresenterImpl;
import com.quickcash.mvp.navigation.NavigationActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DisbursementActivity extends AppCompatActivity implements DisbursementContractor.DisbursementView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.txtViewUploadDocument)
    TextView txtViewUploadDocument;

    @BindView(R.id.txtViewAllDone)
    TextView txtViewAllDone;

    @BindView(R.id.txtViewMinutes)
    TextView txtViewMinutes;

    @BindView(R.id.txtViewFunds)
    TextView txtViewFunds;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    DisbursementAdapter disbursementAdapter;
    DisbursementPresenterImpl disbursementPresenter;
    ArrayList<DisbursementModel.Datum> arrayListDisbursement;
    LinearLayoutManager linearLayoutManager;

    int paymentModeId;
    int loanId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disbursement);
        ButterKnife.bind(this);
        setFont();
        getIntentData();
        arrayListDisbursement = new ArrayList<>();
        disbursementPresenter = new DisbursementPresenterImpl(this, this);
        disbursementAdapter = new DisbursementAdapter(this, arrayListDisbursement);
        getDisbursementDataApi();
    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            loanId = getIntent().getIntExtra(ConstIntent.KEY_LOAN_ID, -1);
        }
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void onInternetError() {
        SnackNotify.checkConnection(onRetryGetDisbursementPayment, relLayMain);
    }

    OnClickInterface onRetryGetDisbursementPayment = new OnClickInterface() {
        @Override
        public void onClick() {
            getDisbursementDataApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void getDisbursementSuccess(ArrayList<DisbursementModel.Datum> arrayListDisbursement) {
        this.arrayListDisbursement = arrayListDisbursement;
        setLayoutManager();
    }

    @Override
    public void onSaveDisbursementSuccess(String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(DisbursementActivity.this, NavigationActivity.class);
                startActivity(intent);
            }
        });
        alertDialog.show();
    }

    @Override
    public void onSaveDisbursementUnsuccess(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void onSaveDisbursementInternetError() {
        SnackNotify.checkConnection(onRetryGetUserAppliedLoan, relLayMain);
    }

    OnClickInterface onRetryGetUserAppliedLoan = new OnClickInterface() {
        @Override
        public void onClick() {
            callingSaveDisbursement();
        }
    };

    public void setPaymentmodeId(int paymentmodeId) {
        this.paymentModeId = paymentmodeId;
    }

    private void getDisbursementDataApi() {
        disbursementPresenter.getDisbursement();
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewUploadDocument, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewAllDone, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewMinutes, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewFunds, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, btnSubmit, FontHelper.FontType.FONT_BOLD);
    }


    private void setLayoutManager() {
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(true);
        setAdapter();
    }

    private void setAdapter() {
        disbursementAdapter = new DisbursementAdapter(this, arrayListDisbursement);
        recyclerView.setAdapter(disbursementAdapter);
    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.btnSubmit)
    public void btnSubmitClicked() {
        callingSaveDisbursement();
    }

    private void callingSaveDisbursement() {
        disbursementPresenter.saveDisbursement(loanId, paymentModeId);
    }
}
