package com.quickcash.mvp.disbursement.presenter;

import android.app.Activity;
import android.provider.Settings;

import com.quickcash.BuildConfig;
import com.quickcash.R;
import com.quickcash.common.helpers.LoginManager;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ApiAdapter;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.session.FcmSession;
import com.quickcash.mvp.common_mvp.CommonModel;
import com.quickcash.mvp.disbursement.DisbursementModel;
import com.quickcash.mvp.loan_detail.LoanDetailModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 18-01-2018.
 */

public class DisbursementPresenterImpl implements DisbursementContractor.Presenter {

    DisbursementContractor.DisbursementView disbursementView;
    Activity activity;
    JSONObject jsonObject;

    public DisbursementPresenterImpl(DisbursementContractor.DisbursementView disbursementView, Activity activity) {
        this.disbursementView = disbursementView;
        this.activity = activity;
    }

    @Override
    public void getDisbursement() {

        try {
            ApiAdapter.getInstance(activity);
            getDisbursementData();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            disbursementView.onInternetError();
        }
    }

    @Override
    public void saveDisbursement(int user_loan_id, int payment_mode_id) {
        try {
            ApiAdapter.getInstance(activity);
            saveDisbursementData( user_loan_id,  payment_mode_id);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            disbursementView.onInternetError();
        }
    }

    private void getDisbursementData() {
        Progress.start(activity);

        int userId = LoginManager.getInstance().getUserData().getUserId();
        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_ID, userId);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE, Const.PARAM_ANDROID);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<DisbursementModel> getOutput = ApiAdapter.getApiService().paymentModeList("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<DisbursementModel>() {
            @Override
            public void onResponse(Call<DisbursementModel> call, Response<DisbursementModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    DisbursementModel disbursementModel = response.body();

                    if (disbursementModel.getStatus() == (Const.PARAM_SUCCESS)) {
                        disbursementView.getDisbursementSuccess(disbursementModel.getData());
                    } else {
                        disbursementView.apiUnsuccess(disbursementModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    disbursementView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<DisbursementModel> call, Throwable t) {
                Progress.stop();
                disbursementView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }

    private void saveDisbursementData(int user_loan_id, int payment_mode_id) {
        Progress.start(activity);

        int userId = LoginManager.getInstance().getUserData().getUserId();
        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_LOAN_ID, user_loan_id);
            jsonObject.put(Const.PARAM_PAYMENT_MODE_ID, payment_mode_id);
            jsonObject.put(Const.PARAM_USER_ID, userId);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE, Const.PARAM_ANDROID);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getOutput = ApiAdapter.getApiService().saveDisbursementtype("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();

                    if (commonModel.getStatus() == (Const.PARAM_SUCCESS)) {
                        disbursementView.onSaveDisbursementSuccess(commonModel.getMessage());
                    } else {
                        disbursementView.onSaveDisbursementUnsuccess(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    disbursementView.onSaveDisbursementUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                disbursementView.onSaveDisbursementUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
