package com.quickcash.mvp.disbursement.presenter;

import com.quickcash.mvp.common_mvp.BaseView;
import com.quickcash.mvp.disbursement.DisbursementModel;
import com.quickcash.mvp.loan_detail.LoanDetailModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 18-01-2018.
 */

public class DisbursementContractor {
    public interface DisbursementView extends BaseView {

        void getDisbursementSuccess(ArrayList<DisbursementModel.Datum> arrayListDisbursement );

        void onSaveDisbursementSuccess(String message);

        void onSaveDisbursementUnsuccess(String message);

        void onSaveDisbursementInternetError();
    }

    public interface Presenter {
        void getDisbursement();
        void saveDisbursement(int user_loan_id,int payment_mode_id);
    }

}
