package com.quickcash.mvp.disbursement;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.plus.model.people.Person;
import com.quickcash.R;
import com.quickcash.common.helpers.FontHelper;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.mvp.loan_packages.LoanPackagesAdapter;
import com.quickcash.mvp.loan_packages.LoanPackagesModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 19-01-2018.
 */

public class DisbursementAdapter extends RecyclerView.Adapter {

    Activity activity;
    ArrayList<DisbursementModel.Datum> raDataArrayListResult;

    public DisbursementAdapter(Activity activity, ArrayList<DisbursementModel.Datum> raDataArrayList) {
        this.activity = activity;
        this.raDataArrayListResult = raDataArrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_disturbment, parent, false);
        return new DisbursementAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final DisbursementModel.Datum result = raDataArrayListResult.get(position);

        ((DisbursementAdapter.Holder) holder).txtViewTitle.setText(result.getTitle());
        ((Holder) holder).txtViewDescription.setText(result.getDescription());

        //check is selected
        if (result.getIsSelected()) {
            ((DisbursementAdapter.Holder) holder).imageViewRadioButton.setImageResource(R.mipmap.ic_radio_fill);
            ((DisbursementActivity) activity).setPaymentmodeId(result.getId());
        } else {
            ((DisbursementAdapter.Holder) holder).imageViewRadioButton.setImageResource(R.mipmap.ic_radio_empty);
        }


        ((DisbursementAdapter.Holder) holder).relLayItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < raDataArrayListResult.size(); i++) {

                    if (i == position) {
                        raDataArrayListResult.get(i).setSelected(true);
                    } else {
                        raDataArrayListResult.get(i).setSelected(false);
                    }
                }
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return raDataArrayListResult.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.imageViewRadioButton)
        ImageView imageViewRadioButton;

        @BindView(R.id.txtViewTitle)
        TextView txtViewTitle;

        @BindView(R.id.txtViewDescription)
        TextView txtViewDescription;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            setFont();
        }

        private void setFont() {
            FontHelper.applyFont(activity, txtViewTitle, FontHelper.FontType.FONT_BOLD);
        }
    }
}
