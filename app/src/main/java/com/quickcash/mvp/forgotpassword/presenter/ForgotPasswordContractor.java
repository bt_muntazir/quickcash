package com.quickcash.mvp.forgotpassword.presenter;

import com.quickcash.mvp.common_mvp.BaseView;

import org.json.JSONObject;

/**
 * Created by Braintech on 12-01-2018.
 */

public class ForgotPasswordContractor {
    public interface ForgotPasswordView extends BaseView {

        void getForgotPasswordSuccess(String message);
    }

    public interface Presenter {
        void getForgotPassword(String email);
    }
}
