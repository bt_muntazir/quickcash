package com.quickcash.mvp.forgotpassword;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.widget.Button;

import com.quickcash.R;
import com.quickcash.common.helpers.FontHelper;
import com.quickcash.common.interfaces.OnClickInterface;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.utility.SnackNotify;
import com.quickcash.common.utility.Utils;
import com.quickcash.mvp.forgotpassword.presenter.ForgotPasswordContractor;
import com.quickcash.mvp.forgotpassword.presenter.ForgotPasswordPresenterImpl;
import com.quickcash.mvp.signup.SignupActivity;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordActivity extends AppCompatActivity implements ForgotPasswordContractor.ForgotPasswordView {

    @BindView(R.id.coordinator)
    CoordinatorLayout coordinator;

    @BindView(R.id.txtViewEnterEmailTitle)
    AppCompatTextView txtViewEnterEmailTitle;

    @BindView(R.id.txtViewForgotPassword)
    AppCompatTextView txtViewForgotPassword;

    @BindView(R.id.edtTextEmail)
    AppCompatEditText edtTextEmail;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @BindView(R.id.txtViewBackToLogin)
    AppCompatTextView txtViewBackToLogin;

    ForgotPasswordPresenterImpl forgotPasswordPresenter;
    JSONObject jsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        forgotPasswordPresenter = new ForgotPasswordPresenterImpl(this, this);
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinator);
    }

    @Override
    public void onInternetError() {

        SnackNotify.checkConnection(onretryForgotPasswordApi, coordinator);
    }

    OnClickInterface onretryForgotPasswordApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getForgotPasswordDataApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinator);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinator);
    }

    @Override
    public void getForgotPasswordSuccess(String message) {
        //SnackNotify.showMessage("Success", coordinator);
        showAlertDialog(message);

    }

     /*----------------------------------------private method--------------------------------*/

    private void showAlertDialog(String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(true);

        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        alertDialog.show();
    }




    private void setFont() {

        FontHelper.setFontFace(txtViewEnterEmailTitle, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewForgotPassword, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(btnSubmit, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewBackToLogin, FontHelper.FontType.FONT_REGULAR, this);
    }

    private void getForgotPasswordDataApi() {
        String email = String.valueOf(edtTextEmail.getText()).trim();

        if (validation(email)) {
            forgotPasswordPresenter.getForgotPassword(email);
        }
    }

    private boolean validation(String email) {
        if (Utils.isEmptyOrNull(email)) {
            //edtTextEmail.setError(getString(R.string.empty_email));
            SnackNotify.showMessage(getString(R.string.empty_email), coordinator);
            return false;
        } else if (!Utils.isValidEmail(email)) {
            //edtTextEmail.setError(getString(R.string.error_email));
            SnackNotify.showMessage(getString(R.string.error_email), coordinator);
            return false;
        }
        return true;
    }


    /*----------------------------------click method---------------------------------------*/
    @OnClick(R.id.txtViewBackToLogin)
    public void txtViewBackToLoginClicked() {

        // hide keyboard
        Utils.hideKeyboardIfOpen(this);
        finish();
    }

    @OnClick(R.id.btnSubmit)
    public void btnSubmitClicked() {
        // hide keyboard
        Utils.hideKeyboardIfOpen(this);
        getForgotPasswordDataApi();
    }


}
