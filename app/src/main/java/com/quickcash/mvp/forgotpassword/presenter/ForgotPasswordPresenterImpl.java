package com.quickcash.mvp.forgotpassword.presenter;

import android.app.Activity;

import com.quickcash.BuildConfig;
import com.quickcash.R;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ApiAdapter;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.mvp.common_mvp.CommonModel;
import com.quickcash.mvp.forgotpassword.ForgotPasswordActivity;
import com.quickcash.mvp.login.presenter.LoginContractor;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 12-01-2018.
 */

public class ForgotPasswordPresenterImpl implements ForgotPasswordContractor.Presenter {

    ForgotPasswordContractor.ForgotPasswordView forgotPasswordView;
    Activity activity;
    JSONObject jsonObject;

    public ForgotPasswordPresenterImpl(ForgotPasswordContractor.ForgotPasswordView forgotPasswordView, Activity activity) {

        this.forgotPasswordView = forgotPasswordView;
        this.activity = activity;
    }

    @Override
    public void getForgotPassword(String email) {

        try {
            ApiAdapter.getInstance(activity);
            getForgotPasswordData(email);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            forgotPasswordView.onInternetError();
        }
    }

    private void getForgotPasswordData(String email) {
        Progress.start(activity);

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_EMAIL, email);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getOutput = ApiAdapter.getApiService().forgotPassword("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();

                    if (commonModel.getStatus()==(Const.PARAM_SUCCESS)) {
                        forgotPasswordView.getForgotPasswordSuccess(commonModel.getMessage());
                    } else {
                        forgotPasswordView.apiUnsuccess(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    forgotPasswordView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                forgotPasswordView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
