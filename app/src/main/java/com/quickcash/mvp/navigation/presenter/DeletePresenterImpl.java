package com.quickcash.mvp.navigation.presenter;

import android.app.Activity;

import com.quickcash.BuildConfig;
import com.quickcash.R;
import com.quickcash.common.helpers.LoginManager;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ApiAdapter;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.mvp.common_mvp.CommonModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 15-01-2018.
 */

public class DeletePresenterImpl implements DeleteContractor.Presenter {

    DeleteContractor.DeleteView deleteView;
    Activity activity;
    JSONObject jsonObject;

    public DeletePresenterImpl(DeleteContractor.DeleteView deleteView, Activity activity) {
        this.deleteView = deleteView;
        this.activity = activity;
    }

    @Override
    public void getDelete() {
        try {
            ApiAdapter.getInstance(activity);
            getDeleteData();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            deleteView.onInternetError();
        }
    }

    private void getDeleteData() {

        Progress.start(activity);

        if (LoginManager.getInstance().isLoggedIn()) {

            int userId = LoginManager.getInstance().getUserData().getUserId();

            try {
                jsonObject = new JSONObject();
                jsonObject.put(Const.PARAM_USER_ID, userId);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }

            final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

            Call<CommonModel> getOutput = ApiAdapter.getApiService().logout("application/json", "no-cache", body);
            getOutput.enqueue(new Callback<CommonModel>() {
                @Override
                public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                    Progress.stop();

                    try {
                        //getting whole data from response
                        CommonModel commonModel = response.body();

                        if (commonModel.getStatus()==(Const.PARAM_SUCCESS)) {
                            deleteView.getDeleteSuccess(commonModel.getMessage());
                        } else {
                            deleteView.apiUnsuccess(commonModel.getMessage());
                        }
                    } catch (NullPointerException exp) {
                        if (BuildConfig.DEBUG)
                            exp.printStackTrace();
                        deleteView.apiUnsuccess(activity.getString(R.string.error_server));
                    }
                }

                @Override
                public void onFailure(Call<CommonModel> call, Throwable t) {
                    Progress.stop();
                    deleteView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            });
        }
    }
}
