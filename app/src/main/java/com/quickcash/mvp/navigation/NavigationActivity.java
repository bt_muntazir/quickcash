package com.quickcash.mvp.navigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.quickcash.R;
import com.quickcash.common.helpers.AlertDialogHelper;
import com.quickcash.common.helpers.LoginManager;
import com.quickcash.common.utility.SnackNotify;
import com.quickcash.common.utility.Utils;
import com.quickcash.mvp.bank_account.BankAccountFragment;
import com.quickcash.mvp.change_password.ChangePasswordActivity;
import com.quickcash.mvp.change_password.ChangePasswordFragment;
import com.quickcash.mvp.dashboard.DashboardFragment;
import com.quickcash.mvp.disbursement.DisbursementActivity;
import com.quickcash.mvp.loan_packages.LoanPackagesActivity;
import com.quickcash.mvp.login.LoginActivity;
import com.quickcash.mvp.my_loans.MyLoansActivity;
import com.quickcash.mvp.mycash_application.MyCashApplicationActivity;
import com.quickcash.mvp.navigation.presenter.DeleteContractor;
import com.quickcash.mvp.navigation.presenter.DeletePresenterImpl;
import com.quickcash.mvp.profile.ProfileFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NavigationActivity extends AppCompatActivity implements DeleteContractor.DeleteView {
    @BindView(R.id.imgViewMenu)
    ImageView imgViewMenu;

    @BindView(R.id.imgViewNotification)
    ImageView imgViewNotification;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;

    @BindView(R.id.txtViewDashboard)
    TextView txtViewDashboard;

    @BindView(R.id.txtViewMyProfile)
    TextView txtViewMyProfile;

    @BindView(R.id.txtViewMyBankAccounts)
    TextView txtViewMyBankAccounts;

    @BindView(R.id.txtViewMyLoans)
    TextView txtViewMyLoans;

    @BindView(R.id.txtViewApplyCash)
    TextView txtViewApplyCash;

    @BindView(R.id.txtViewMyCashApplication)
    TextView txtViewMyCashApplication;

    @BindView(R.id.txtViewChangePassword)
    TextView txtViewChangePassword;

    @BindView(R.id.txtViewLogout)
    TextView txtViewLogout;

    @BindView(R.id.frameLayout)
    FrameLayout frameLayout;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    DeletePresenterImpl deletePresenter;

    boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        ButterKnife.bind(this);
        fragmentManager = getSupportFragmentManager();
        deletePresenter = new DeletePresenterImpl(this, this);

        imgViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                } else {
                    drawer_layout.openDrawer(GravityCompat.START);
                }
            }
        });
        addDashboardFragment();
    }

    @Override
    public void onBackPressed() {
        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }*/

       /* DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1&&getSupportFragmentManager().getFragments().get) {
                finish();
            } else {
                super.onBackPressed();
            }
        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment dashboardFragment = (DashboardFragment) fragmentManager.findFragmentByTag(getString(R.string.nav_dashboard));
            if (fragmentManager.getBackStackEntryCount() > 1) {
                if (Utils.isVisibleOrNull(dashboardFragment)) {
                    finish();
                } else {
                    fragmentManager.popBackStack();
                }
            } else {
                finish();
            }
        }
    }


    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, drawer_layout);
    }

    @Override
    public void onInternetError() {
    }

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, drawer_layout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, drawer_layout);
    }

    @Override
    public void getDeleteSuccess(String message) {
        LoginManager.getInstance().logoutUser();
        startActivity(new Intent(this, LoginActivity.class));
        finishAffinity();
    }


    private void drawerClose() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        }
    }

    @OnClick(R.id.imgViewMenu)
    public void click() {
        drawerClose();
    }

    @OnClick(R.id.txtViewDashboard)
    public void txtViewDashboard() {
        drawerClose();
        DashboardFragment dashboardFragment = new DashboardFragment();
        String dashBoard = getString(R.string.nav_dashboard);
        replaceFragment(dashboardFragment, dashBoard);
    }

    @OnClick(R.id.txtViewMyProfile)
    public void txtViewMyProfileClick() {
        profileCalling();
    }

    @OnClick(R.id.txtViewMyBankAccounts)
    public void txtViewMyBankAccountsClick() {
        bankAc();
    }

    @OnClick(R.id.txtViewMyLoans)
    public void txtViewMyLoansClick() {
        drawerClose();
        Intent intent = new Intent(this, MyLoansActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.txtViewApplyCash)
    public void txtViewApplyCashClick() {
        Intent intent = new Intent(this, LoanPackagesActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.txtViewMyCashApplication)
    public void txtViewMyCashApplicationClick() {
        Intent intent = new Intent(this, MyCashApplicationActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.txtViewChangePassword)
    public void txtViewChangePasswordClick() {
        drawerClose();
        if (LoginManager.getInstance().isLoggedIn()) {
           /* ChangePasswordFragment changePasswordFragment = new ChangePasswordFragment();
            String changepassword = getString(R.string.nav_change_password);
            replaceFragment(changePasswordFragment, changepassword);*/
            Intent intent = new Intent(this, ChangePasswordActivity.class);
            startActivity(intent);
        }
    }

    @OnClick(R.id.txtViewLogout)
    public void txtViewLogoutClick() {
        drawerClose();
        if (LoginManager.getInstance().isLoggedIn()) {
            AlertDialogHelper.showMessageToLogout(this);
        }
    }

    public void getLogoutApi() {
        deletePresenter.getDelete();
    }

    private void addDashboardFragment() {

        fragmentTransaction = fragmentManager.beginTransaction();
        DashboardFragment dashboardFragment = new DashboardFragment();
        String dashboard = this.getString(R.string.nav_dashboard);
        fragmentTransaction.add(R.id.frameLayout, dashboardFragment, dashboard);
        fragmentTransaction.addToBackStack(dashboard);
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void replaceFragment(Fragment fragment, String tag) {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment, tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void profileCalling() {
        drawerClose();
        ProfileFragment profileFragment = new ProfileFragment();
        String profFragment = getString(R.string.nav_myprofile);
        replaceFragment(profileFragment, profFragment);
    }

    public void bankAc() {
        drawerClose();
        BankAccountFragment bankAccountFragment = new BankAccountFragment();
        String bankAccount = getString(R.string.nav_my_bank_account);
        replaceFragment(bankAccountFragment, bankAccount);
    }

    public void openDashboard() {
        drawerClose();
        DashboardFragment dashboardFragment = new DashboardFragment();
        String dashboard = this.getString(R.string.nav_dashboard);
        replaceFragment(dashboardFragment, dashboard);
    }


}
