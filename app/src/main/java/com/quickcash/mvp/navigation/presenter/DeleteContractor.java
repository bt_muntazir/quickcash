package com.quickcash.mvp.navigation.presenter;

import com.quickcash.mvp.common_mvp.BaseView;

/**
 * Created by Braintech on 15-01-2018.
 */

public class DeleteContractor {
    public interface DeleteView extends BaseView {

        void getDeleteSuccess(String message);
    }

    public interface Presenter {
        void getDelete();
    }
}
