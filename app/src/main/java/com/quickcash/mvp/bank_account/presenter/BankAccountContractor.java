package com.quickcash.mvp.bank_account.presenter;


import com.quickcash.mvp.bank_account.BankAccountModel;
import com.quickcash.mvp.common_mvp.BaseView;

import java.util.ArrayList;

/**
 * Created by Braintech on 16-01-2018.
 */

public class BankAccountContractor {

    public interface BankDetailView extends BaseView {

        void saveBankDetailSuccess(String message);
        void getBankDetailSuccess(ArrayList<BankAccountModel.Datum> model);
        void saveBankDetailInternetError();
    }

    public interface Presenter {
        void saveBankDetail(String bankName,String ifsc,String accountNo,String accountType);
        void getBankDetail();
    }
}
//cbdhshj