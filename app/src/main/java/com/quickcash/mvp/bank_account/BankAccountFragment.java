package com.quickcash.mvp.bank_account;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.quickcash.R;
import com.quickcash.common.interfaces.OnClickInterface;
import com.quickcash.common.utility.SnackNotify;
import com.quickcash.common.utility.Utils;
import com.quickcash.mvp.bank_account.presenter.BancAccountPresenterImpl;
import com.quickcash.mvp.bank_account.presenter.BankAccountContractor;
import com.quickcash.mvp.navigation.NavigationActivity;
import com.quickcash.mvp.profile.presenter.ProfilePresenterImpl;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;


public class BankAccountFragment extends Fragment implements BankAccountContractor.BankDetailView {

    @BindView(R.id.frameLayout)
    FrameLayout frameLayout;

    @BindView(R.id.txtViewMyProfileTitle)
    AppCompatTextView txtViewMyProfileTitle;

    @BindView(R.id.txtViewDescription)
    AppCompatTextView txtViewDescription;

    @BindView(R.id.txtViewAccountType)
    AppCompatTextView txtViewAccountType;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

    @BindView(R.id.radioSaving)
    RadioButton radioSaving;

    @BindView(R.id.radioCurrent)
    RadioButton radioCurrent;

    @BindView(R.id.edtTextBankName)
    AppCompatEditText edtTextBankName;

    @BindView(R.id.edtTextIFSC)
    AppCompatEditText edtTextIFSC;

    @BindView(R.id.edtTextAccountNo)
    AppCompatEditText edtTextAccountNo;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    ArrayList<BankAccountModel.Datum> datumArrayList;
    BancAccountPresenterImpl bancAccountPresenter;
    String accountType = "Saving";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bank_account, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bancAccountPresenter = new BancAccountPresenterImpl(this, getActivity());
        callingGetApi();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, frameLayout);
    }

    @Override
    public void onInternetError() {

        SnackNotify.checkConnection(onretryGetBankDetailApi, frameLayout);
    }

    OnClickInterface onretryGetBankDetailApi = new OnClickInterface() {
        @Override
        public void onClick() {
            callingGetApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        //SnackNotify.showMessage(message, frameLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, frameLayout);
    }

    @Override
    public void saveBankDetailSuccess(String message) {
        showAlertDialog(message);
    }

    @Override
    public void getBankDetailSuccess(ArrayList<BankAccountModel.Datum> datumArrayList) {

        if (datumArrayList.size() > 0) {
            BankAccountModel.Datum datum = datumArrayList.get(0);

            if (datum.getAccountType().equals(accountType)) {
                radioSaving.setChecked(true);
                radioCurrent.setChecked(false);
            } else {
                radioCurrent.setChecked(true);
                radioSaving.setChecked(false);
            }
            edtTextBankName.setText(datum.getBankName());
            edtTextAccountNo.setText(datum.getAccountNo());
            edtTextIFSC.setText(datum.getIfscCode());

        }
    }

    @Override
    public void saveBankDetailInternetError() {

        SnackNotify.checkConnection(onretrySaveBankDetailApi, frameLayout);
    }

    OnClickInterface onretrySaveBankDetailApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getBankDataApi();
        }
    };

    /*----------------------------private method---------------------------------*/

    private void callingGetApi() {
        bancAccountPresenter.getBankDetail();
    }


    private void showAlertDialog(String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(true);

        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ((NavigationActivity) getActivity()).openDashboard();
                dialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void getBankDataApi() {
        String bankName = String.valueOf(edtTextBankName.getText());
        String bankIfsc = String.valueOf(edtTextIFSC.getText());
        String accountNo = String.valueOf(edtTextAccountNo.getText());

        if (validation(bankName, bankIfsc, accountNo)) {
            bancAccountPresenter.saveBankDetail(bankName, bankIfsc, accountNo, accountType);
        }
    }


    private boolean validation(String bankName, String bankIfsc, String accountNo) {
        if (Utils.isEmptyOrNull(bankName)) {
            SnackNotify.showMessage(getString(R.string.empty_bank_name), frameLayout);
            return false;
        } else if (Utils.isEmptyOrNull(bankIfsc)) {
            SnackNotify.showMessage(getString(R.string.empty_ifsc), frameLayout);
            return false;
        } else if (Utils.isEmptyOrNull(accountNo)) {
            SnackNotify.showMessage(getString(R.string.empty_account_no), frameLayout);
            return false;

        }
        return true;
    }

      /*----------------------------Click method---------------------------------*/

    @OnClick(R.id.btnSubmit)
    public void btnSubmit() {
//hide keyboard
        Utils.hideKeyboardIfOpen(getActivity());
        getBankDataApi();
    }

    @OnCheckedChanged({R.id.radioCurrent, R.id.radioSaving})
    public void onRadioButtonCheckChanged(CompoundButton button, boolean checked) {
        if (checked) {
            switch (button.getId()) {
                case R.id.radioCurrent:
                    accountType = "Current";
                    break;
                case R.id.radioSaving:
                    accountType = "Saving";
                    break;
            }
        }
    }
}