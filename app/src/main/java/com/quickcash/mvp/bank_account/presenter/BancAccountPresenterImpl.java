package com.quickcash.mvp.bank_account.presenter;

import android.app.Activity;
import android.provider.Settings;
import android.util.Log;

import com.quickcash.BuildConfig;
import com.quickcash.R;
import com.quickcash.common.helpers.LoginManager;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ApiAdapter;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.session.FcmSession;
import com.quickcash.mvp.bank_account.BankAccountModel;
import com.quickcash.mvp.common_mvp.CommonModel;
import com.quickcash.mvp.forgotpassword.presenter.ForgotPasswordContractor;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 16-01-2018.
 */

public class BancAccountPresenterImpl implements BankAccountContractor.Presenter {

    BankAccountContractor.BankDetailView bankDetailView;
    Activity activity;
    JSONObject jsonObject;

    public BancAccountPresenterImpl(BankAccountContractor.BankDetailView bankDetailView, Activity activity) {

        this.bankDetailView = bankDetailView;
        this.activity = activity;
    }

    @Override
    public void saveBankDetail(String bankName, String ifsc, String accountNo, String accountType) {
        try {
            ApiAdapter.getInstance(activity);
            getBankAccountData(bankName, ifsc, accountNo, accountType);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            bankDetailView.onInternetError();
        }
    }


    private void getBankAccountData(String bankName, String ifsc, String accountNo, String accountType) {
        Progress.start(activity);

        int userid= LoginManager.getInstance().getUserData().getUserId();

        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_ID, userid);
            jsonObject.put(Const.PARAM_ACCOUNT_TYPE, accountType);
            jsonObject.put(Const.PARAM_ACCOUNT_NO, accountNo);
            jsonObject.put(Const.PARAM_IFSC_CODE, ifsc);
            jsonObject.put(Const.PARAM_BANK_NAME, bankName);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE, "A");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getOutput = ApiAdapter.getApiService().saveBankAccount("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();

                    if (commonModel.getStatus()==(Const.PARAM_SUCCESS)) {
                        bankDetailView.saveBankDetailSuccess(commonModel.getMessage());
                    } else {
                        bankDetailView.apiUnsuccess(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    bankDetailView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                bankDetailView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }

    @Override
    public void getBankDetail() {
        try {
            ApiAdapter.getInstance(activity);
            getResult();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            bankDetailView.onInternetError();
        }
    }

    private void getResult(){
        Progress.start(activity);

        int userid= LoginManager.getInstance().getUserData().getUserId();

        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_ID, userid);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE, "A");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<BankAccountModel> getOutput = ApiAdapter.getApiService().getBankAccount("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<BankAccountModel>() {
            @Override
            public void onResponse(Call<BankAccountModel> call, Response<BankAccountModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    BankAccountModel bankAccountModel = response.body();

                    if (bankAccountModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        bankDetailView.getBankDetailSuccess(bankAccountModel.getData());
                    } else {
                        bankDetailView.apiUnsuccess(bankAccountModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    bankDetailView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<BankAccountModel> call, Throwable t) {
                Progress.stop();
                bankDetailView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }

}
