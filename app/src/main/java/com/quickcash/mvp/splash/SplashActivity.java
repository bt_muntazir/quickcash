package com.quickcash.mvp.splash;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;


import com.quickcash.R;
import com.quickcash.common.helpers.AlertDialogHelper;
import com.quickcash.common.helpers.FontHelper;
import com.quickcash.common.helpers.LoginManager;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ConstSession;
import com.quickcash.common.utility.PrefUtil;
import com.quickcash.mvp.navigation.NavigationActivity;
import com.quickcash.mvp.login.LoginActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity implements LocationListener {

   /* @BindView(R.id.txtViewNagasFinance)
    AppCompatTextView txtViewNagasFinance;*/

    @BindView(R.id.txtViewFinanceLicance)
    AppCompatTextView txtViewFinanceLicance;

    @BindView(R.id.txtViewNagasPhilipines)
    AppCompatTextView txtViewNagasPhilipines;

    @BindView(R.id.txtViewPoweredBy)
    AppCompatTextView txtViewPoweredBy;

    private int SPLASH_TIME_OUT = 1000;

    private LocationManager locationManager;

    double latitude;
    double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        //setFont();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getCurrentLocation();
            }
        }, SPLASH_TIME_OUT);

        // get user location

    }

    private void setFont() {

        //FontHelper.setFontFace(txtViewFastApproval, FontHelper.FontType.FONT_REGULAR, this);
        // FontHelper.setFontFace(txtViewNagasFinance, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewFinanceLicance, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewNagasPhilipines, FontHelper.FontType.FONT_REGULAR, this);

    }

    private void getCurrentLocation() {
        checkRuntimePermission();
    }


    private void getLocation() {

        //setIntent();
        Progress.start(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean networkLocationEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean gpsLocationEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        if (networkLocationEnabled || gpsLocationEnabled) {

            if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER) && networkLocationEnabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 2000, 2000, this);
                locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            } else if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER) && gpsLocationEnabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 2000, 2000, this);
                locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }
    }

   /* @Override
    protected void onResume() {
        super.onResume();
        checkRuntimePermission();
    }*/

    @Override
    protected void onRestart() {
        super.onRestart();
        checkRuntimePermission();
    }

   /* private void checkRuntimePermission() {

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {

                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {

                            Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();

                            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                //isInitializedMap = true;
                                if (!isRequestForLocPermission) {

                                    isRequestForLocPermission = true;

                                    AlertDialogHelper.alertEnableLocation(SplashActivity.this);
                                }
                                // return;
                            } else {
                                getLocation();
                            }
                        } else {

                            isRequestForLocPermission = true;

                            // show alert dialog navigating to Settings
                            AlertDialogHelper.alertEnableAppSetting(SplashActivity.this);
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            AlertDialogHelper.alertEnableAppSetting(SplashActivity.this);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                        Toast.makeText(getApplicationContext(), "onPermissionRationaleShouldBeShown! ", Toast.LENGTH_SHORT).show();

                        if (!isRequestForLocPermission)
                            token.continuePermissionRequest();
                        else
                            AlertDialogHelper.alertEnableLocation(SplashActivity.this);
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                //.onSameThread()
                .check();

    }*/

    private void checkRuntimePermission() {

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {

                        // permission is granted
                        // Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();

                        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            AlertDialogHelper.alertEnableLocation(SplashActivity.this);
                        } else {
                            getLocation();
                        }
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            AlertDialogHelper.alertEnableAppSetting(SplashActivity.this);
                        } else {
                            checkRuntimePermission();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }


    @Override
    public void onLocationChanged(Location location) {

        Progress.stop();

        latitude = location.getLatitude();
        longitude = location.getLongitude();

        PrefUtil.putString(this, ConstSession.PREF_LATITUIDE, String.valueOf(latitude));
        PrefUtil.putString(this, ConstSession.PREF_LONGITUDE, String.valueOf(longitude));

        Log.e("latitude", "------>" + latitude);
        Log.e("longitude", "------>" + longitude);

        if (LoginManager.getInstance().isLoggedIn()) {

            Intent intent = new Intent(SplashActivity.this, NavigationActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }


    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

}
