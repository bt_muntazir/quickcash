package com.quickcash.mvp.loan_packages;

import android.app.Activity;
import android.provider.Settings;

import com.quickcash.BuildConfig;
import com.quickcash.R;
import com.quickcash.common.helpers.LoginManager;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ApiAdapter;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.requestresponse.ConstSession;
import com.quickcash.common.session.FcmSession;
import com.quickcash.common.utility.PrefUtil;
import com.quickcash.mvp.login.model.LoginResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by BRAINTECH on 15-01-2018.
 */

public class LoanPackagesPresenterImpl implements LoanPackagesPresenter {

    Activity activity;
    LoanPackagesView loanPackagesView;
    JSONObject jsonObject;

    public LoanPackagesPresenterImpl(Activity activity, LoanPackagesView loanPackagesView) {
        this.activity = activity;
        this.loanPackagesView = loanPackagesView;
    }

    @Override
    public void callingLoanPackagesApi() {
        try {
            ApiAdapter.getInstance(activity);
            gettingResultOfLoanPackages();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            loanPackagesView.onInternetError();
        }
    }

    private void gettingResultOfLoanPackages() {
        Progress.start(activity);

        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();
        int userId= LoginManager.getInstance().getUserData().getUserId();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_ID,userId);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE, "A");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }


        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<LoanPackagesModel> getOutput = ApiAdapter.getApiService().loanPackages("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<LoanPackagesModel>() {
            @Override
            public void onResponse(Call<LoanPackagesModel> call, Response<LoanPackagesModel> response) {

                Progress.stop();

               try {
                    //getting whole data from response
                   LoanPackagesModel loanPackagesModel = response.body();
                   if (loanPackagesModel.getStatus()==(Const.PARAM_SUCCESS)) {
                        loanPackagesView.onSuccessLoanPackages(loanPackagesModel.getData());
                    } else {
                        loanPackagesView.onUnsuccessLoanPackages(loanPackagesModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    loanPackagesView.onUnsuccessLoanPackages(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<LoanPackagesModel> call, Throwable t) {
                Progress.stop();
                loanPackagesView.onUnsuccessLoanPackages(activity.getString(R.string.error_server));
            }
        });
    }
}
