package com.quickcash.mvp.loan_packages;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quickcash.R;
import com.quickcash.common.helpers.AlertDialogHelper;
import com.quickcash.common.helpers.FontHelper;
import com.quickcash.common.interfaces.OnClickInterface;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.requestresponse.ConstIntent;
import com.quickcash.common.utility.SnackNotify;
import com.quickcash.mvp.loan_detail.LoanDetailActivity;
import com.quickcash.mvp.loan_detail.LoanDetailModel;
import com.quickcash.mvp.loan_detail.presenter.LoanDetailContractor;
import com.quickcash.mvp.loan_detail.presenter.LoanDetailPresenterImpl;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by BRAINTECH on 15-01-2018.
 */

public class LoanPackagesAdapter extends RecyclerView.Adapter<LoanPackagesAdapter.Holder> implements LoanDetailContractor.LoanDetailView {


    Activity activity;
    ArrayList<LoanPackagesModel.Datum> datum;

    LoanDetailPresenterImpl loanDetailPresenter;
    RelativeLayout relRoot;

    int loanId;


    public LoanPackagesAdapter(Activity activity, ArrayList<LoanPackagesModel.Datum> datum, RelativeLayout relRoot) {
        this.activity = activity;
        this.datum = datum;
        this.relRoot = relRoot;

        loanDetailPresenter = new LoanDetailPresenterImpl(this, activity);
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_loan_packages, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        final LoanPackagesModel.Datum result = datum.get(position);

        holder.txtViewTime.setText(result.getMaxDuration());
        holder.txtViewAmount.setText(Const.KEY_PRICE_SYMBOL + " "+result.getLoanAmount());
        holder.txtViewInterest.setText(result.getInterest().get(0).getInterestRate() + "%");
        holder.txtViewProcessingFees.setText(Const.KEY_PRICE_SYMBOL +" "+ result.getProcessingFees());
        holder.txtViewPackageName.setText(result.getPackage_name());

        holder.txtViewApplyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loanId = result.getId();
                getLoanDetailDataApi();

            }
        });
    }

    @Override
    public int getItemCount() {
        return datum.size();
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onInternetError() {
        SnackNotify.checkConnection(onRetryDetail, relRoot);
    }

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, relRoot);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, relRoot);
    }

    @Override
    public void getLoanDetailSuccess(LoanDetailModel.Data loanDetailModel) {

        Intent intent = new Intent(activity, LoanDetailActivity.class);
        intent.putExtra(ConstIntent.KEY_DATA, loanDetailModel);
        activity.startActivity(intent);

    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewTime)
        TextView txtViewTime;

        @BindView(R.id.txtViewTitleTime)
        TextView txtViewTitleTime;

        @BindView(R.id.txtViewAmount)
        TextView txtViewAmount;

        @BindView(R.id.txtViewTitleAmount)
        TextView txtViewTitleAmount;

        @BindView(R.id.txtViewInterest)
        TextView txtViewInterest;

        @BindView(R.id.txtViewTitleInterest)
        TextView txtViewTitleInterest;

        @BindView(R.id.txtViewTitleProcessingFees)
        TextView txtViewTitleProcessingFees;

        @BindView(R.id.txtViewProcessingFees)
        TextView txtViewProcessingFees;

        @BindView(R.id.txtViewApplyNow)
        TextView txtViewApplyNow;

        @BindView(R.id.txtViewMoreInfo)
        TextView txtViewMoreInfo;

        @BindView(R.id.txtViewTitlePackageName)
        TextView txtViewTitlePackageName;

        @BindView(R.id.txtViewPackageName)
        TextView txtViewPackageName;



        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            setFont();
        }

        private void setFont() {
            FontHelper.applyFont(activity, txtViewTime, FontHelper.FontType.FONT_BOLD);
            FontHelper.applyFont(activity, txtViewAmount, FontHelper.FontType.FONT_BOLD);
            FontHelper.applyFont(activity, txtViewInterest, FontHelper.FontType.FONT_BOLD);
            FontHelper.applyFont(activity, txtViewTitleTime, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewTitleAmount, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewTitleInterest, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewTitleProcessingFees, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewProcessingFees, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewApplyNow, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewMoreInfo, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewTitlePackageName, FontHelper.FontType.FONT_REGULAR);
            FontHelper.applyFont(activity, txtViewPackageName, FontHelper.FontType.FONT_REGULAR);

        }
    }

    private void getLoanDetailDataApi() {
        loanDetailPresenter.getLoanDetail(loanId);
    }

    OnClickInterface onRetryDetail = new OnClickInterface() {
        @Override
        public void onClick() {
            getLoanDetailDataApi();
        }
    };
}
