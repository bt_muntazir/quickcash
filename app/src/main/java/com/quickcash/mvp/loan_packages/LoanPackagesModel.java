package com.quickcash.mvp.loan_packages;

/**
 * Created by BRAINTECH on 15-01-2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LoanPackagesModel {


    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }


    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;

        @SerializedName("package_name")
        @Expose
        private String package_name;

        @SerializedName("loan_amount")
        @Expose
        private String loanAmount;

        @SerializedName("interest")
        @Expose
        private ArrayList<Interest> interest = null;

        @SerializedName("processing_fees")
        @Expose
        private String processingFees;

        @SerializedName("max_duration")
        @Expose
        private String maxDuration;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getPackage_name() {
            return package_name;
        }

        public void setPackage_name(String package_name) {
            this.package_name = package_name;
        }

        public String getLoanAmount() {
            return loanAmount;
        }

        public void setLoanAmount(String loanAmount) {
            this.loanAmount = loanAmount;
        }

        public ArrayList<Interest> getInterest() {
            return interest;
        }

        public void setInterest(ArrayList<Interest> interest) {
            this.interest = interest;
        }

        public String getProcessingFees() {
            return processingFees;
        }

        public void setProcessingFees(String processingFees) {
            this.processingFees = processingFees;
        }

        public String getMaxDuration() {
            return maxDuration;
        }

        public void setMaxDuration(String maxDuration) {
            this.maxDuration = maxDuration;
        }

        public class Interest {

            @SerializedName("interest_rate")
            @Expose
            private String interestRate;
            @SerializedName("tenure_id")
            @Expose
            private Integer tenureId;
            @SerializedName("tenure_title")
            @Expose
            private String tenureTitle;

            public String getInterestRate() {
                return interestRate;
            }

            public void setInterestRate(String interestRate) {
                this.interestRate = interestRate;
            }

            public Integer getTenureId() {
                return tenureId;
            }

            public void setTenureId(Integer tenureId) {
                this.tenureId = tenureId;
            }

            public String getTenureTitle() {
                return tenureTitle;
            }

            public void setTenureTitle(String tenureTitle) {
                this.tenureTitle = tenureTitle;
            }


        }

    }
}



