package com.quickcash.mvp.loan_packages;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quickcash.R;
import com.quickcash.common.helpers.FontHelper;
import com.quickcash.common.interfaces.OnClickInterface;
import com.quickcash.common.utility.SnackNotify;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoanPackagesActivity extends AppCompatActivity implements LoanPackagesView {

    @BindView(R.id.relRoot)
    RelativeLayout relRoot;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.imgViewNotification)
    ImageView imgViewNotification;

    @BindView(R.id.txtViewAvailableLoans)
    TextView txtViewAvailableLoans;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    LoanPackagesPresenterImpl loanPackagesPresenterImpl;
    ArrayList<LoanPackagesModel.Datum> datum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_packages);
        ButterKnife.bind(this);
        callingLoanPackagesApi();
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewAvailableLoans, FontHelper.FontType.FONT_BOLD);
    }

    private void callingLoanPackagesApi() {
        loanPackagesPresenterImpl = new LoanPackagesPresenterImpl(this, this);
        loanPackagesPresenterImpl.callingLoanPackagesApi();
    }


    @Override
    public void onSuccessLoanPackages(ArrayList<LoanPackagesModel.Datum> datum) {
        this.datum = datum;
        setLayoutManager();
    }

    private void setLayoutManager() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        setAdapter();

    }

    private void setAdapter() {
        LoanPackagesAdapter loanPackagesAdapter = new LoanPackagesAdapter(this, datum, relRoot);
        recyclerView.setAdapter(loanPackagesAdapter);
    }

    @Override
    public void onUnsuccessLoanPackages(String message) {
        SnackNotify.showMessage(message, relRoot);
    }

    @Override
    public void onInternetError() {
        SnackNotify.checkConnection(onRetryLoanPackageApi, relRoot);
    }

    OnClickInterface onRetryLoanPackageApi = new OnClickInterface() {
        @Override
        public void onClick() {
            callingLoanPackagesApi();
        }
    };

    @OnClick(R.id.imgViewBack)
    public void imgViewBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.imgViewNotification)
    public void imgViewNotification() {

    }
}
