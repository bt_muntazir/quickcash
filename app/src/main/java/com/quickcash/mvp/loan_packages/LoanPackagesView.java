package com.quickcash.mvp.loan_packages;

import java.util.ArrayList;

/**
 * Created by BRAINTECH on 15-01-2018.
 */

public interface LoanPackagesView {

    public void onSuccessLoanPackages(ArrayList<LoanPackagesModel.Datum> datum);
    public void onUnsuccessLoanPackages(String message);
    public void onInternetError();
}
