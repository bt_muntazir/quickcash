package com.quickcash.mvp.change_password;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.quickcash.R;
import com.quickcash.common.utility.SnackNotify;
import com.quickcash.common.utility.Utils;
import com.quickcash.mvp.change_password.presenter.ChangePasswordContractor;
import com.quickcash.mvp.change_password.presenter.ChangePasswordPresenterImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ChangePasswordFragment extends Fragment implements ChangePasswordContractor.ChangePasswordView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.txtViewChangePassword)
    AppCompatTextView txtViewChangePassword;

    @BindView(R.id.edtTextNewPassword)
    AppCompatEditText edtTextNewPassword;

    @BindView(R.id.edtTextConfirmPassword)
    AppCompatEditText edtTextConfirmPassword;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

  /*  @BindView(R.id.txtViewBackToLogin)
    AppCompatTextView txtViewBackToLogin;

    @BindView(R.id.txtViewPoweredBy)
    AppCompatTextView txtViewPoweredBy;*/

    ChangePasswordPresenterImpl changePasswordPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        ButterKnife.bind(this, view);
        changePasswordPresenter = new ChangePasswordPresenterImpl(this, getActivity());
        return view;
    }


    @Override
    public void onError(String message) {

    }

    @Override
    public void onInternetError() {

    }

    @Override
    public void apiUnsuccess(String message) {

    }

    @Override
    public void serverNotResponding(String message) {

    }

    @Override
    public void getChangePasswordSuccess(String message) {

    }

    private boolean validation(String email, String password) {
        if (Utils.isEmptyOrNull(email)) {
            SnackNotify.showMessage(getString(R.string.empty_new_password), relLayMain);
            return false;
        } else if (Utils.isEmptyOrNull(password)) {
            SnackNotify.showMessage(getString(R.string.empty_confirm_password), relLayMain);
            return false;
        }
        return true;
    }


    @OnClick(R.id.btnSubmit)
    public void btnSubmitClick() {
        // hide keyboard
        Utils.hideKeyboardIfOpen(getActivity());

        String oldPassword = String.valueOf("12345678");
        String newPass = String.valueOf(edtTextNewPassword.getText());
        String conPass = String.valueOf(edtTextConfirmPassword.getText());
        if (validation(newPass, conPass)) {
            changePasswordPresenter.getChangePassword(oldPassword, newPass, conPass);
        }
    }
}
