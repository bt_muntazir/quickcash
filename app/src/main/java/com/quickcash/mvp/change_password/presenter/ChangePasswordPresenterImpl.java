package com.quickcash.mvp.change_password.presenter;

import android.app.Activity;
import android.provider.Settings;

import com.quickcash.BuildConfig;
import com.quickcash.R;
import com.quickcash.common.helpers.LoginManager;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ApiAdapter;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.session.FcmSession;
import com.quickcash.mvp.common_mvp.CommonModel;
import com.quickcash.mvp.disbursement.DisbursementModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 22-01-2018.
 */

public class ChangePasswordPresenterImpl implements ChangePasswordContractor.Presenter {

    ChangePasswordContractor.ChangePasswordView changePasswordView;
    Activity activity;
    JSONObject jsonObject;

    public ChangePasswordPresenterImpl(ChangePasswordContractor.ChangePasswordView changePasswordView, Activity activity) {
        this.changePasswordView = changePasswordView;
        this.activity = activity;
    }

    @Override
    public void getChangePassword(String oldPassword, String newPass, String confirmPass) {
        try {
            ApiAdapter.getInstance(activity);
            getChangePasswordData(oldPassword, newPass, confirmPass);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            changePasswordView.onInternetError();
        }
    }

    private void getChangePasswordData(String oldPassword, String newPass, String confirmPass) {
        Progress.start(activity);

        int userId = LoginManager.getInstance().getUserData().getUserId();
        String email = LoginManager.getInstance().getUserData().getEmail();
        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_ID, userId);
            jsonObject.put(Const.PARAM_PASSWORD, newPass);
            jsonObject.put(Const.PARAM_OLD_PASSWORD, oldPassword);
            jsonObject.put(Const.PARAM_CONFIRM_PASSWORD, confirmPass);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE, Const.PARAM_ANDROID);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getOutput = ApiAdapter.getApiService().changePassword("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();

                    if (commonModel.getStatus() == (Const.PARAM_SUCCESS)) {
                        changePasswordView.getChangePasswordSuccess(commonModel.getMessage());
                    } else {
                        changePasswordView.apiUnsuccess(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    changePasswordView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                changePasswordView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
