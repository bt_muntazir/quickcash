package com.quickcash.mvp.change_password.presenter;

import com.quickcash.mvp.common_mvp.BaseView;
import com.quickcash.mvp.disbursement.DisbursementModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 22-01-2018.
 */

public class ChangePasswordContractor {

    public interface ChangePasswordView extends BaseView {

        void getChangePasswordSuccess(String message);
    }

    public interface Presenter {
        void getChangePassword(String oldPassword,String newPass,String confirmPass);
    }
}
