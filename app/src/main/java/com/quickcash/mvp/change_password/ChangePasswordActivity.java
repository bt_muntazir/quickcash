package com.quickcash.mvp.change_password;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.quickcash.R;
import com.quickcash.common.utility.SnackNotify;
import com.quickcash.common.utility.Utils;
import com.quickcash.mvp.change_password.presenter.ChangePasswordContractor;
import com.quickcash.mvp.change_password.presenter.ChangePasswordPresenterImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordActivity extends AppCompatActivity implements ChangePasswordContractor.ChangePasswordView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.txtViewChangePassword)
    AppCompatTextView txtViewChangePassword;

    @BindView(R.id.edtTextNewPassword)
    AppCompatEditText edtTextNewPassword;

    @BindView(R.id.edtTextConfirmPassword)
    AppCompatEditText edtTextConfirmPassword;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @BindView(R.id.txtViewBack)
    AppCompatTextView txtViewBack;

    @BindView(R.id.txtViewPoweredBy)
    AppCompatTextView txtViewPoweredBy;

    ChangePasswordPresenterImpl changePasswordPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        changePasswordPresenter = new ChangePasswordPresenterImpl(this, this);
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onInternetError() {

    }

    @Override
    public void apiUnsuccess(String message) {

    }

    @Override
    public void serverNotResponding(String message) {

    }

    @Override
    public void getChangePasswordSuccess(String message) {

    }

    private boolean validation(String email, String password) {
        if (Utils.isEmptyOrNull(email)) {
            SnackNotify.showMessage(getString(R.string.empty_new_password), relLayMain);
            return false;
        } else if (Utils.isEmptyOrNull(password)) {
            SnackNotify.showMessage(getString(R.string.empty_confirm_password), relLayMain);
            return false;
        }
        return true;
    }


    @OnClick(R.id.btnSubmit)
    public void btnSubmitClick() {
        // hide keyboard
        Utils.hideKeyboardIfOpen(this);

        String oldPassword = String.valueOf("12345678");
        String newPass = String.valueOf(edtTextNewPassword.getText());
        String conPass = String.valueOf(edtTextConfirmPassword.getText());
        if (validation(newPass, conPass)) {
            changePasswordPresenter.getChangePassword(oldPassword, newPass, conPass);
        }
    }

    @OnClick(R.id.txtViewBack)
    public void txtViewBackClick(){
        finish();
    }


}
