package com.quickcash.mvp.login.presenter;

import com.quickcash.mvp.common_mvp.BaseView;
import com.quickcash.mvp.login.model.LoginResponseModel;

import org.json.JSONObject;

/**
 * Created by Braintech on 12-01-2018.
 */

public class LoginContractor {
    public interface LoginView extends BaseView {

        void getLoginSuccess(LoginResponseModel.Data loginResponseModel);
    }

    public interface Presenter {
        void getLogin(String email,String password);
    }
}
