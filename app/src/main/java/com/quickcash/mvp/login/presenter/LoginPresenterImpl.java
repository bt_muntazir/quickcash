package com.quickcash.mvp.login.presenter;

import android.app.Activity;
import android.provider.Settings;

import com.quickcash.BuildConfig;
import com.quickcash.R;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ApiAdapter;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.requestresponse.ConstSession;
import com.quickcash.common.session.FcmSession;
import com.quickcash.common.utility.PrefUtil;
import com.quickcash.mvp.login.model.LoginResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.AccessibleObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 12-01-2018.
 */

public class LoginPresenterImpl implements LoginContractor.Presenter {

    LoginContractor.LoginView loginView;
    Activity activity;
    JSONObject jsonObject;

    public LoginPresenterImpl(LoginContractor.LoginView loginView, Activity activity) {

        this.loginView = loginView;
        this.activity = activity;
    }

    @Override
    public void getLogin(String email,String password) {

        try {
            ApiAdapter.getInstance(activity);
            getLoginData(email,password);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            loginView.onInternetError();
        }
    }

    private void getLoginData(String email,String password) {
        Progress.start(activity);

        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();
        String currentLatitude = PrefUtil.getString(activity, ConstSession.PREF_LATITUIDE, "");
        String currentLongitude = PrefUtil.getString(activity, ConstSession.PREF_LONGITUDE, "");

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_EMAIL, email);
            jsonObject.put(Const.PARAM_PASSWORD, password);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE, "A");
            jsonObject.put(Const.PARAM_CURRENT_LAT, currentLatitude);
            jsonObject.put(Const.PARAM_CURRENT_LONG, currentLongitude);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }


        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<LoginResponseModel> getOutput = ApiAdapter.getApiService().login("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    LoginResponseModel loginModel = response.body();

                    if (loginModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        loginView.getLoginSuccess(loginModel.getData());
                    } else {
                        loginView.apiUnsuccess(loginModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    loginView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                Progress.stop();
                loginView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
