package com.quickcash.mvp.login;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.method.PasswordTransformationMethod;
import android.widget.Button;
import android.widget.ImageView;

import com.quickcash.R;
import com.quickcash.common.helpers.FontHelper;
import com.quickcash.common.helpers.LoginManager;
import com.quickcash.common.interfaces.OnClickInterface;
import com.quickcash.common.utility.SnackNotify;
import com.quickcash.common.utility.Utils;
import com.quickcash.mvp.navigation.NavigationActivity;
import com.quickcash.mvp.forgotpassword.ForgotPasswordActivity;
import com.quickcash.mvp.login.model.LoginResponseModel;
import com.quickcash.mvp.login.presenter.LoginContractor;
import com.quickcash.mvp.login.presenter.LoginPresenterImpl;
import com.quickcash.mvp.signup.SignupActivity;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class LoginActivity extends AppCompatActivity implements LoginContractor.LoginView {

    @BindView(R.id.coordinator)
    CoordinatorLayout coordinator;

    @BindView(R.id.txtViewLoginTitle)
    AppCompatTextView txtViewLoginTitle;

    @BindView(R.id.edtTextEmail)
    AppCompatEditText edtTextEmail;

    @BindView(R.id.edtTextPassword)
    AppCompatEditText edtTextPassword;

    @BindView(R.id.btnLogin)
    Button btnLogin;

    @BindView(R.id.txtViewDontHaveAccount)
    AppCompatTextView txtViewDontHaveAccount;

    @BindView(R.id.txtViewRegister)
    AppCompatTextView txtViewRegister;

    @BindView(R.id.txtViewForgotPassword)
    AppCompatTextView txtViewForgotPassword;

    @BindView(R.id.imgViewEye)
    ImageView imgViewEye;

    LoginPresenterImpl loginPresenter;

    JSONObject jsonObject;

    boolean flag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        loginPresenter = new LoginPresenterImpl(this, this);
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinator);
    }

    @Override
    public void onInternetError() {

        SnackNotify.checkConnection(onretryLoginApi, coordinator);
    }

    OnClickInterface onretryLoginApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getLoginDataApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinator);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinator);
    }

    @Override
    public void getLoginSuccess(LoginResponseModel.Data loginResponseModel) {
        SnackNotify.showMessage("Success", coordinator);

        LoginManager.getInstance().createLoginSession(loginResponseModel);

        Intent intent = new Intent(LoginActivity.this, NavigationActivity.class);
        startActivity(intent);
        finish();
    }

    /*----------------------------------------private method--------------------------------*/

    private void setFont() {

        FontHelper.setFontFace(txtViewLoginTitle, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(btnLogin, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewRegister, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewDontHaveAccount, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewForgotPassword, FontHelper.FontType.FONT_REGULAR, this);
    }

    private void getLoginDataApi() {

        String email = edtTextEmail.getText().toString().trim();
        String password = String.valueOf(edtTextPassword.getText()).trim();

        if (validation(email, password)) {
            loginPresenter.getLogin(email, password);
        }
    }

    private boolean validation(String email, String password) {
        if (Utils.isEmptyOrNull(email)) {
            SnackNotify.showMessage(getString(R.string.empty_email), coordinator);
            return false;
        } else if (!Utils.isValidEmail(email)) {
            SnackNotify.showMessage(getString(R.string.error_email), coordinator);
            return false;
        } else if (Utils.isEmptyOrNull(password)) {
            SnackNotify.showMessage(getString(R.string.empty_password), coordinator);
            return false;

        }
        return true;
    }


    /*----------------------------------click method---------------------------------------*/
    @OnClick(R.id.txtViewRegister)
    public void txtViewRegisterClicked() {

        startActivity(new Intent(this, SignupActivity.class));
    }

    @OnClick(R.id.txtViewForgotPassword)
    public void txtViewForgotPasswordClicked() {

        startActivity(new Intent(this, ForgotPasswordActivity.class));
    }

    @OnClick(R.id.btnLogin)
    public void btnLoginClicked() {
        // hide keyboard
        Utils.hideKeyboardIfOpen(this);
        getLoginDataApi();
    }


    @OnClick(R.id.imgViewEye)
    public void imgViewEye() {
        if (flag) {
            edtTextPassword.setTransformationMethod(new PasswordTransformationMethod());
            int length = edtTextPassword.getText().toString().length();
            edtTextPassword.setSelection(length);
            flag = false;
            imgViewEye.setImageResource(R.mipmap.ic_password_show);
        } else {
            edtTextPassword.setTransformationMethod(null);
            int length = edtTextPassword.getText().toString().length();
            edtTextPassword.setSelection(length);
            flag = true;
            imgViewEye.setImageResource(R.mipmap.ic_password_hide);
        }


    }
}
