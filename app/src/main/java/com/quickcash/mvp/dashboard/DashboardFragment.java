package com.quickcash.mvp.dashboard;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quickcash.R;
import com.quickcash.mvp.loan_packages.LoanPackagesActivity;
import com.quickcash.mvp.mycash_application.MyCashApplicationActivity;
import com.quickcash.mvp.navigation.NavigationActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DashboardFragment extends Fragment {


    @BindView(R.id.relLayMyProfile)
    RelativeLayout relLayMyProfile;

    @BindView(R.id.txtViewMyProfile)
    TextView txtViewMyProfile;

    @BindView(R.id.relLayBankAC)
    RelativeLayout relLayBankAC;

    @BindView(R.id.txtViewBankAcc)
    TextView txtViewBankAcc;

    @BindView(R.id.relLayApplyCash)
    RelativeLayout relLayApplyCash;

    @BindView(R.id.txtViewApplyCash)
    TextView txtViewApplyCash;

    @BindView(R.id.relLayMyCashApplication)
    RelativeLayout relLayMyCashApplication;

    @BindView(R.id.txtViewMyCashApplication)
    TextView txtViewMyCashApplication;

    @BindView(R.id.txtViewGetCashUpto)
    TextView txtViewGetCashUpto;

    @BindView(R.id.txtViewNoBankAccountRequired)
    TextView txtViewNoBankAccountRequired;

    @BindView(R.id.btnApplyNow)
    Button btnApplyNow;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @OnClick({R.id.relLayApplyCash,R.id.btnApplyNow})
    public void relLayApplyCashClick(){
        Intent intent=new Intent(getActivity(), LoanPackagesActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.relLayMyCashApplication)
    public void relLayMyCashApplication(){
        Intent intent=new Intent(getActivity(), MyCashApplicationActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.relLayMyProfile)
    public void relLayMyProfile(){
        ((NavigationActivity)getActivity()).profileCalling();
    }

    @OnClick(R.id.relLayBankAC)
    public void relLayBankAC(){
        ((NavigationActivity)getActivity()).bankAc();
    }
}
