package com.quickcash.mvp.upload_document;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.quickcash.R;
import com.quickcash.common.helpers.AlertDialogHelper;
import com.quickcash.common.helpers.FontHelper;
import com.quickcash.common.helpers.PathUtil;
import com.quickcash.common.helpers.UploadPhotoHelper;
import com.quickcash.common.requestresponse.ConstIntent;
import com.quickcash.common.utility.SnackNotify;
import com.quickcash.common.utility.Utils;
import com.quickcash.mvp.loan_detail.LoanDetailModel;
import com.quickcash.mvp.profile.ProfileModel;
import com.quickcash.mvp.profile.presenter.ProfileContractor;
import com.quickcash.mvp.profile.presenter.ProfilePresenterImpl;
import com.quickcash.mvp.splash.SplashActivity;
import com.quickcash.mvp.upload_document.presenter.UploadDocumentContractor;
import com.quickcash.mvp.upload_document.presenter.UploadDocumentPresenterImpl;
import com.quickcash.mvp.verification_and_authenication.VerificationAndAuthenicationActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UploadDocumentActivity extends AppCompatActivity implements UploadDocumentContractor.UploadDocumentView, ProfileContractor.ProfileView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.txtViewUploadDocument)
    TextView txtViewUploadDocument;

    @BindView(R.id.txtViewAlmostDone)
    TextView txtViewAlmostDone;

    @BindView(R.id.txtViewAlmostDoneDesc)
    TextView txtViewAlmostDoneDesc;

    @BindView(R.id.txtViewMyPhoto)
    TextView txtViewMyPhoto;

    @BindView(R.id.txtViewMyCapture)
    TextView txtViewMyCapture;

    @BindView(R.id.txtViewPanCard)
    TextView txtViewPanCard;

    @BindView(R.id.txtViewPanCardCapture)
    TextView txtViewPanCardCapture;

    @BindView(R.id.txtViewBankEStatement)
    TextView txtViewBankEStatement;

    @BindView(R.id.txtViewBankEStatementUpload)
    TextView txtViewBankEStatementUpload;

    @BindView(R.id.txtViewAddressProof)
    TextView txtViewAddressProof;

    @BindView(R.id.txtViewAddressProofUpload)
    TextView txtViewAddressProofUpload;

    @BindView(R.id.btnContinue)
    Button btnContinue;

    @BindView(R.id.imgViewPhotoTick)
    ImageView imgViewPhotoTick;

    @BindView(R.id.imgViewPanCardTick)
    ImageView imgViewPanCardTick;

    @BindView(R.id.imgViewStatementTick)
    ImageView imgViewStatementTick;

    @BindView(R.id.imgViewAddressTick)
    ImageView imgViewAddressTick;

    ArrayList<ProfileModel.Datum> arrayListProfileModel;
    int loanId;
    int tenureInterestId;
    boolean flag = false;


    /*******************Camera and gallery use***********************/
    // CameraImageAdapter cameraImageAdapter;
    // ArrayList<ImageUploadModal> imageList;
    ArrayList<String> imagePathList;


    byte[] imgInByteArray;
    byte[] panCardByteArray;
    byte[] bankStatementByteArray;
    byte[] addressProofByteArray;

    private int REQUEST_CAMERA = 999;
    private static int RESULT_LOAD_IMG = 123;

    boolean isUploadingImage = false;
    File photo = null;
    Bitmap photoBitmap = null;
    Uri mImageUri;

    String imageUpload = "";
    String panCard = "";
    String bankEStatement = "";
    String addressProof = "";
    int bit = -1;

    UploadDocumentPresenterImpl uploadDocumentPresenter;
    ProfilePresenterImpl profilePresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_document);
        ButterKnife.bind(this);

        uploadDocumentPresenter = new UploadDocumentPresenterImpl(this, this);
        profilePresenter = new ProfilePresenterImpl(this, this);

        getIntentData();

        getProfileDataApi();

        if (savedInstanceState != null) {
            try {
                mImageUri = Uri.parse(savedInstanceState.getString("media_url"));
            } catch (NullPointerException ee) {
                ee.printStackTrace();
            }
        }

        setFont();
    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            loanId = getIntent().getIntExtra(ConstIntent.KEY_LOAN_ID, -1);
            tenureInterestId = getIntent().getIntExtra(ConstIntent.KEY_TENURE_INTEREST_ID, -1);
        }
    }

    /*----------------------------------private method----------------------------------------*/

    private void setFont() {
        FontHelper.applyFont(this, txtViewUploadDocument, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewAlmostDone, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewAlmostDoneDesc, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewMyPhoto, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewMyCapture, FontHelper.FontType.FONT_THIN);
        FontHelper.applyFont(this, txtViewPanCard, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewPanCardCapture, FontHelper.FontType.FONT_THIN);
        FontHelper.applyFont(this, txtViewBankEStatement, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewBankEStatementUpload, FontHelper.FontType.FONT_THIN);
        FontHelper.applyFont(this, txtViewAddressProof, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewAddressProofUpload, FontHelper.FontType.FONT_THIN);
    }

    /*--------------------------------click method-----------------------------------*/
    @OnClick(R.id.relLayMyProfile)
    public void txtViewMyCaptureClicked() {

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                            openCamera();

                        } else {
                            // navigate user to app settings
                            AlertDialogHelper.alertEnableAppSetting(UploadDocumentActivity.this);
                        }

                        // check for permanent denial of any permission
                        /*if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }*/
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();



       /* Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted, open the camera
                        openCamera();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            // navigate user to app settings
                            AlertDialogHelper.alertEnableAppSetting(UploadDocumentActivity.this);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();*/

    }

    @OnClick(R.id.relLayPanCard)
    public void txtViewPanCardCaptureClicked() {

        bit = 0;

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                            pickFile();

                        } else {
                            // navigate user to app settings
                            AlertDialogHelper.alertEnableAppSetting(UploadDocumentActivity.this);
                        }

                        // check for permanent denial of any permission
                        /*if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }*/
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();

    }

    @OnClick(R.id.relLayBankEstatement)
    public void txtViewBankEStatementUploadClicked() {
        bit = 1;

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                            pickFile();

                        } else {
                            // navigate user to app settings
                            AlertDialogHelper.alertEnableAppSetting(UploadDocumentActivity.this);
                        }

                        // check for permanent denial of any permission
                        /*if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }*/
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();

    }

    @OnClick(R.id.relLayAddressProof)
    public void txtViewAddressProofUploadClicked() {
        bit = 2;

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                            pickFile();

                        } else {
                            // navigate user to app settings
                            AlertDialogHelper.alertEnableAppSetting(UploadDocumentActivity.this);
                        }

                        // check for permanent denial of any permission
                        /*if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }*/
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    public void openCamera() {

        isUploadingImage = true;
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            // place where to store camera taken picture
            photo = UploadPhotoHelper.createTemporaryFile("picture", ".JPEG");
        } catch (Exception e) {
            e.printStackTrace();
            // Log.v("pic", "Can't create file to take picture!");
        }
        mImageUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", photo);

        // require permission for below 21
        List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            grantUriPermission(packageName, mImageUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }

        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);

        //start camera intent
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void pickFile() {

        String[] mimeTypes =
                {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                        "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                        "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                        "text/plain",
                        "application/pdf",
                        "image/*"
                };

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";
            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }
            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }
        startActivityForResult(Intent.createChooser(intent, "ChooseFile"), RESULT_LOAD_IMG);


//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//        intent.setType("*/*");
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
//
//        try {
//            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), RESULT_LOAD_IMG);
//        } catch (android.content.ActivityNotFoundException ex) {
//            // Potentially direct the user to the Market with a Dialog
//            Toast.makeText(this, "Please install a File Manager.", Toast.LENGTH_SHORT).show();
//        }
    }

    public void getActivityResult(Intent data, int requestCode) {
        try {
            if (requestCode == RESULT_LOAD_IMG) {
                onPickedFile(data);
            } else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onPickedFile(Intent data) {

        // Get the Uri of the selected file
        Uri uri = data.getData();
        Log.e("uri", "File Uri: " + uri.toString());

        // String path = PathUtil.getPath(this, uri);
        String filename = Utils.getFileName(this, uri);

        if (bit == 0) {
            //panCard = UploadPhotoHelper.getRealPathFromURI(String.valueOf(uri), this);
            panCard = filename;
            Log.e("Pan card", panCard);
            panCardByteArray = convertion(uri);
            Log.e("PanCard Array", "" + panCardByteArray.length);
        } else if (bit == 1) {
            bankEStatement = filename;
            Log.e("Bank Statement", bankEStatement);
            bankStatementByteArray = convertion(uri);
            Log.e("Bank Statement Array", "" + bankStatementByteArray.length);
        } else if (bit == 2) {
            addressProof = filename;
            Log.e("addressProof", addressProof);
            addressProofByteArray = convertion(uri);
            Log.e("Address Array", "" + addressProofByteArray.length);
        }

           /* if (path != null) {
                String mimeType = Utils.getMimeType(path);

                if (mimeType != null) {

                    try {
                        //String fileName = path.substring(path.lastIndexOf("/"));
                        // String actualMimeType = path.substring(path.lastIndexOf("/"));


                    } catch (ArrayIndexOutOfBoundsException ee) {
                        ee.printStackTrace();
                        Toast.makeText(this, "Failed to load file! Please choose valid file.", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(this, "Failed to load file! Please choose valid file.", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                Toast.makeText(this, "Failed to load file! Please choose valid file.", Toast.LENGTH_SHORT).show();
            }*/


        /*// Get the path
        Bitmap bitmap = null;

        try {
            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);

            String imagePathResized = UploadPhotoHelper.getAbsolutePath(bitmap);

            if (bit == 0) {
                panCard = UploadPhotoHelper.getRealPathFromURI(String.valueOf(uri), this);
                panCardByteArray = convertion(uri);
            } else if (bit == 1) {
                bankEStatement = UploadPhotoHelper.getRealPathFromURI(String.valueOf(uri), this);
                bankStatementByteArray = convertion(uri);

            } else if (bit == 2) {
                addressProof = UploadPhotoHelper.getRealPathFromURI(String.valueOf(uri), this);
                addressProofByteArray = convertion(uri);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
*/


       /* if (path != null) {
            if (bit == 0) {
                panCard = path;
                panCardByteArray = convertion(uri);

            } else if (bit == 1) {
                bankEStatement = path;
                bankStatementByteArray = convertion(uri);

            } else if (bit == 2) {
                addressProof = path;
                addressProofByteArray = convertion(uri);
            }
            Log.e("path", "File Path: " + path);
        }*/
    }


    private byte[] convertion(Uri uri) {
        byte[] byteArray = null;
        try {
            InputStream is = getContentResolver().openInputStream(uri);
            try {
                byteArray = Utils.getBytes(is);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return byteArray;
    }


    private void onCaptureImageResult(Intent data) {

        String path = null;
        String absolutePath = null;
        float rotate = 0;

        ContentResolver cr = getContentResolver();
        cr.notifyChange(mImageUri, null);


        absolutePath = new File(mImageUri.getPath()).getAbsoluteFile().toString();
        path = new File(mImageUri.getPath()).toString();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            rotate = UploadPhotoHelper.getOrientation(photo.getPath());
        else
            rotate = UploadPhotoHelper.getOrientation(path);


        Bitmap bitmap = null;

        try {
            bitmap = UploadPhotoHelper.rotateImage(MediaStore.Images.Media.getBitmap(cr, mImageUri), rotate);
        } catch (Exception e) {
            Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();

        }
        try {
            photoBitmap = UploadPhotoHelper.resizeImageForImageView(bitmap, rotate);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                addImageToList(photo.getAbsolutePath(), photoBitmap);
            else
                addImageToList(absolutePath, photoBitmap);


        } catch (OutOfMemoryError ex) {
            ex.printStackTrace();
        }
    }

    public void addImageToList(String imagePath, Bitmap bitmap) {

        if (bitmap != null) {

            // sd card image
            String imagePathResized = UploadPhotoHelper.getAbsolutePath(bitmap);
            Log.e("Image Path ", "------>" + imagePathResized);
            imageUpload = imagePathResized;

            Uri uri = Uri.fromFile(new File(imagePathResized));

            try {
                InputStream is = getContentResolver().openInputStream(uri);

                try {
                    imgInByteArray = Utils.getBytes(is);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        } else {
            SnackNotify.showMessage("Please select image.", relLayMain);
        }
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (mImageUri != null)
            // Save the user's current state
            savedInstanceState.putString("media_url", mImageUri.toString());
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            getActivityResult(data, requestCode);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }

    }


    @OnClick(R.id.btnContinue)
    public void btnContinueClicked() {
        // startActivity(new Intent(this, VerificationAndAuthenicationActivity.class));
        if (flag) {
            intent();
        } else {
            uploadDocumentPresenter.saveUploadDocument(imgInByteArray, imageUpload, panCardByteArray,
                    panCard, bankStatementByteArray, bankEStatement, addressProofByteArray, addressProof);
        }
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onInternetError() {

    }

    @Override
    public void apiUnsuccess(String message) {
        AlertDialogHelper.showMessage(this, message);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void getUploadDocumentSuccess(String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Message
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                intent();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private void intent() {
        Intent intent = new Intent(UploadDocumentActivity.this, VerificationAndAuthenicationActivity.class);
        intent.putExtra(ConstIntent.KEY_LOAN_ID, loanId);
        intent.putExtra(ConstIntent.KEY_TENURE_INTEREST_ID, tenureInterestId);
        startActivity(intent);
    }

    @Override
    public void alert(String message) {
        AlertDialogHelper.showMessage(this, message);
    }

    @Override
    public void getProfileSuccess(ArrayList<ProfileModel.Datum> profileModel) {
        arrayListProfileModel = profileModel;

        String photo = arrayListProfileModel.get(0).getPhoto();
        String pancard = arrayListProfileModel.get(0).getPanCard();
        String bankStatement = arrayListProfileModel.get(0).getBankStatement();
        String address = arrayListProfileModel.get(0).getAddressProof();

        if (photo.length() > 0) {
            imgViewPhotoTick.setVisibility(View.VISIBLE);
            flag = true;
        } else {
            imgViewPhotoTick.setVisibility(View.GONE);
            flag = false;
        }

        if (pancard.length() > 0) {
            imgViewPanCardTick.setVisibility(View.VISIBLE);
            flag = true;
        } else {
            imgViewPanCardTick.setVisibility(View.GONE);
            flag = false;
        }

        if (bankStatement.length() > 0) {
            imgViewStatementTick.setVisibility(View.VISIBLE);
            flag = true;
        } else {
            imgViewStatementTick.setVisibility(View.GONE);
            flag = false;
        }


        if (address.length() > 0) {
            imgViewAddressTick.setVisibility(View.VISIBLE);
            flag = true;
        } else {
            imgViewAddressTick.setVisibility(View.GONE);
            flag = false;
        }
    }

    @Override
    public void onSaveSuccess(String message) {

    }

    @Override
    public void onSaveUnsuccess(String message) {

    }

    @Override
    public void onSaveInternetError() {

    }

    private void getProfileDataApi() {

        profilePresenter.getProfile();
    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
