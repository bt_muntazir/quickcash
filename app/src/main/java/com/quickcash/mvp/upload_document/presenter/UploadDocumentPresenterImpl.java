package com.quickcash.mvp.upload_document.presenter;

import android.app.Activity;
import android.provider.Settings;

import com.quickcash.BuildConfig;
import com.quickcash.R;
import com.quickcash.common.helpers.LoginManager;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ApiAdapter;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.requestresponse.ConstSession;
import com.quickcash.common.session.FcmSession;
import com.quickcash.common.utility.PrefUtil;
import com.quickcash.mvp.common_mvp.CommonModel;
import com.quickcash.mvp.login.model.LoginResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.AccessibleObject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 18-01-2018.
 */

public class UploadDocumentPresenterImpl implements UploadDocumentContractor.Presenter {

    UploadDocumentContractor.UploadDocumentView uploadDocumentView;
    Activity activity;
    JSONObject jsonObject;

    public UploadDocumentPresenterImpl(UploadDocumentContractor.UploadDocumentView uploadDocumentView, Activity activity) {
        this.uploadDocumentView = uploadDocumentView;
        this.activity = activity;
    }


    @Override
    public void saveUploadDocument(byte[] photo, String strPhoto, byte[] panCard, String strPanCard, byte[] bankStatement,
                                   String strBankStatement, byte[] addressProof, String strAddressProof) {
        try {
            ApiAdapter.getInstance(activity);
            if (validation(photo, panCard, bankStatement, addressProof)) {
                uploadPhoto(photo, strPhoto, panCard, strPanCard, bankStatement, strBankStatement, addressProof, strAddressProof);
            }
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            uploadDocumentView.onInternetError();
        }
    }

    private boolean validation(byte[] photo, byte[] panCard, byte[] bankStatement, byte[] addressProof) {
        if (photo == null) {
            uploadDocumentView.alert("Please provide your photo.");
            return false;
        } else if (panCard == null) {
            uploadDocumentView.alert("Please provide pan Card.");
            return false;
        } else if (bankStatement == null) {
            uploadDocumentView.alert("Please provide bank statement.");
            return false;
        } else if (addressProof == null) {
            uploadDocumentView.alert("Please provide address proof.");
            return false;
        }
        return true;
    }


    private void uploadPhoto(byte[] photo, String strPhoto, byte[] panCard, String strPanCard, byte[] bankStatement,
                             String strBankStatement, byte[] addressProof, String strAddressProof) {

        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();
        int userId = LoginManager.getInstance().getUserData().getUserId();


        RequestBody requestFilePhoto, requestFilePanCard, requestFileBankStatement, requestFileAddressProof;

        requestFilePhoto = RequestBody.create(MediaType.parse("*/*"), photo);
        requestFilePanCard = RequestBody.create(MediaType.parse("*/*"), panCard);
        requestFileBankStatement = RequestBody.create(MediaType.parse("*/*"), bankStatement);
        requestFileAddressProof = RequestBody.create(MediaType.parse("*/*"), addressProof);

        MultipartBody.Part bodyPhoto = MultipartBody.Part.createFormData(Const.PARAM_PHOTO, strPhoto, requestFilePhoto);
        MultipartBody.Part bodyPanCard = MultipartBody.Part.createFormData(Const.PARAM_PAN_CARD, strPanCard, requestFilePanCard);
        MultipartBody.Part bodyBankStatement = MultipartBody.Part.createFormData(Const.PARAM_BANK_STATEMENT, strBankStatement, requestFileBankStatement);
        MultipartBody.Part bodyAddressProof = MultipartBody.Part.createFormData(Const.PARAM_ADDRESS_PROOF, strAddressProof, requestFileAddressProof);

        RequestBody reqUserId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userId));
        RequestBody reqDeviceId = RequestBody.create(MediaType.parse("text/*"), deviceId);
        RequestBody reqDeviceToken = RequestBody.create(MediaType.parse("text/*"), deviceToken);
        RequestBody reqDeviceType = RequestBody.create(MediaType.parse("text/*"), Const.PARAM_ANDROID);
        RequestBody reqLoanId = RequestBody.create(MediaType.parse("text/*"), "1");


        Progress.start(activity);
        Call<CommonModel> getLoginOutput = ApiAdapter.getApiService().uploadDocument(bodyPhoto, bodyPanCard, bodyBankStatement,
                bodyAddressProof, reqUserId, reqLoanId, reqDeviceId, reqDeviceToken, reqDeviceType);

        getLoginOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();
                    String message = commonModel.getMessage();

                    if (commonModel.getStatus()==1) {
                        uploadDocumentView.getUploadDocumentSuccess(message);
                    } else {
                        uploadDocumentView.apiUnsuccess(message);
                    }

                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    uploadDocumentView.serverNotResponding(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                uploadDocumentView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }

   /* MultipartBody.Builder builder = new MultipartBody.Builder();
    builder.setType(MultipartBody.FORM);
    builder.addFormDataPart("event_name", eventName);
    builder.addFormDataPart("location", loacation);
    builder.addFormDataPart("longitude", longitude);
    builder.addFormDataPart("latitude", latitude);
    builder.addFormDataPart("is_private", isPublic);
    builder.addFormDataPart("caption", caption);
    RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), coverImage);
    builder.addFormDataPart("cover_image", coverImage.getName(), requestBody);
    RequestBody requestBody1 = null;

    for (int i = 0, size = eventFiles.size(); i < size; i++) {
        requestBody1 = RequestBody.create(MediaType.parse("multipart/form-data"), eventFiles.get(i));
        builder.addFormDataPart("album_images" + "[" + i + "]", eventFiles.get(i).getName(), requestBody1);
    }
    RequestBody finalRequestBody = builder.build();

    final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(RestClient.ROOT)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    RestClient.NetworkCall networkCall = retrofit.create(RestClient.NetworkCall.class);

    Call<EventResponse> response = networkCall.uploadEvent(Prefs.getAuth(App.getContext()), finalRequestBody);

    response.enqueue(new Callback<EventResponse>() {
        @Override
        public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(MY_NOTIFICATION_ID);
            if (response.code() == Constant.SUCCESS_STATUS) {

                if (response.body() != null) {
                    sendBroadcast(new Intent(Constant.REFRESH_FEEDS));
                    Log.e("Success", "Success");
                }
            } else {
                Converter<ResponseBody, EventResponse> converter =
                        retrofit.responseBodyConverter(EventResponse.class, new Annotation[0]);
                try {
                    if (response.errorBody() != null) {
                        EventResponse errorResponse = converter.convert(response.errorBody());
                        Log.e("Failure", "Failure");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<EventResponse> call, Throwable t) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(MY_NOTIFICATION_ID);
            Log.e("Failure", "Failure");
        }
    });*/


}
