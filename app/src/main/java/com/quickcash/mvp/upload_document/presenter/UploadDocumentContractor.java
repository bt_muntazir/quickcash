package com.quickcash.mvp.upload_document.presenter;

import com.quickcash.mvp.common_mvp.BaseView;
import com.quickcash.mvp.login.model.LoginResponseModel;

/**
 * Created by Braintech on 18-01-2018.
 */

public class UploadDocumentContractor {

    public interface UploadDocumentView extends BaseView {

        void alert(String message);

        void getUploadDocumentSuccess(String message);
    }

    public interface Presenter {
        void saveUploadDocument(byte[] photo, String strPhoto, byte[] panCard, String strPanCard, byte[] bankStatement,
                                String strBankStatement, byte[] addressProof, String strAddressProof);
    }
}
