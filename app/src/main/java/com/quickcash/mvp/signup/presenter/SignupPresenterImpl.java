package com.quickcash.mvp.signup.presenter;

import android.app.Activity;

import com.quickcash.BuildConfig;
import com.quickcash.R;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ApiAdapter;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.mvp.common_mvp.CommonModel;

import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 12-01-2018.
 */

public class SignupPresenterImpl implements SignupContractor.Presenter {

    SignupContractor.SignupView signupView;
    Activity activity;
    JSONObject jsonObject;

    public SignupPresenterImpl(SignupContractor.SignupView signupView, Activity activity) {
        this.signupView = signupView;
        this.activity = activity;
    }

    @Override
    public void getSignup(JSONObject jsonObject) {

        try {
            ApiAdapter.getInstance(activity);
            getSignupResult(jsonObject);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            signupView.onInternetError();
        }
    }

    private void getSignupResult(JSONObject jsonObject) {
        Progress.start(activity);

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getOutput = ApiAdapter.getApiService().signup("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();

                    if (commonModel.getStatus()==(Const.PARAM_SUCCESS)) {
                        signupView.getSignupSuccess(commonModel.getMessage());
                    } else {
                        signupView.apiUnsuccess(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    signupView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                signupView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
