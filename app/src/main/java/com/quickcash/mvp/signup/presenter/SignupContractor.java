package com.quickcash.mvp.signup.presenter;

import com.quickcash.mvp.common_mvp.BaseView;

import org.json.JSONObject;

/**
 * Created by Braintech on 12-01-2018.
 */

public class SignupContractor {
    public interface SignupView extends BaseView {

        void getSignupSuccess(String message);
    }

    public interface Presenter {
        void getSignup(JSONObject jsonObject);
    }
}
