package com.quickcash.mvp.signup;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.widget.Button;
import android.widget.LinearLayout;

import com.quickcash.R;
import com.quickcash.common.helpers.FontHelper;
import com.quickcash.common.interfaces.OnClickInterface;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.requestresponse.ConstSession;
import com.quickcash.common.session.FcmSession;
import com.quickcash.common.utility.PrefUtil;
import com.quickcash.common.utility.SnackNotify;
import com.quickcash.common.utility.Utils;
import com.quickcash.mvp.common_mvp.AdapterSpinner;
import com.quickcash.mvp.common_mvp.DateFragment;
import com.quickcash.mvp.login.LoginActivity;
import com.quickcash.mvp.signup.presenter.SignupContractor;
import com.quickcash.mvp.signup.presenter.SignupPresenterImpl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class SignupActivity extends AppCompatActivity implements SignupContractor.SignupView {

    @BindView(R.id.coordinator)
    CoordinatorLayout coordinator;

    @BindView(R.id.txtViewSignupTitle)
    AppCompatTextView txtViewSignupTitle;

    @BindView(R.id.edtTextFirstName)
    AppCompatEditText edtTextFirstName;

    @BindView(R.id.edtTextLastName)
    AppCompatEditText edtTextLastName;

    @BindView(R.id.linLayGender)
    LinearLayout linLayGender;

    @BindView(R.id.spinnerGender)
    AppCompatSpinner spinnerGender;

    @BindView(R.id.txtViewDob)
    AppCompatTextView txtViewDob;

    @BindView(R.id.edtTextCurrentAdd)
    AppCompatEditText edtTextCurrentAdd;

    @BindView(R.id.edtTextPhoneNo)
    AppCompatEditText edtTextPhoneNo;

    @BindView(R.id.edtTextEmail)
    AppCompatEditText edtTextEmail;

    @BindView(R.id.edtTextPassword)
    AppCompatEditText edtTextPassword;

    @BindView(R.id.edtTextPinCode)
    AppCompatEditText edtTextPinCode;

    @BindView(R.id.edtMonthlySalary)
    AppCompatEditText edtMonthlySalary;

    @BindView(R.id.edtTextPanCardNo)
    AppCompatEditText edtTextPanCardNo;

    @BindView(R.id.btnSignUp)
    Button btnSignUp;

    @BindView(R.id.txtViewAlreadyMember)
    AppCompatTextView txtViewAlreadyMember;

    @BindView(R.id.txtViewLoginTitle)
    AppCompatTextView txtViewLoginTitle;

    SignupPresenterImpl signupPresenter;
    JSONObject jsonObject;
    AdapterSpinner adapterSpinner;
    ArrayList<HashMap<String, String>> arrayListGender;
    String selectedGender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        signupPresenter = new SignupPresenterImpl(this, this);
        setInitialAdapter();
        getGenders();

    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinator);
    }

    @Override
    public void onInternetError() {

        SnackNotify.checkConnection(onretrySignupApi, coordinator);
    }

    OnClickInterface onretrySignupApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getSignupDataApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinator);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinator);
    }

    @Override
    public void getSignupSuccess(String message) {
        showAlertDialog(message);
    }

      /*----------------------------------------private method--------------------------------*/

    private void showAlertDialog(String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(true);

        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        alertDialog.show();
    }

    private void setInitialAdapter() {
        arrayListGender = new ArrayList<>();
        HashMap hashMapFirstIndexCountry = new HashMap();
        hashMapFirstIndexCountry.put(Const.KEY_ID, 0);
        hashMapFirstIndexCountry.put(Const.KEY_NAME, getString(R.string.hint_gender));
        arrayListGender.add(hashMapFirstIndexCountry);

        setAdapter();
    }

    private void setFont() {

        FontHelper.setFontFace(txtViewSignupTitle, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(btnSignUp, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewAlreadyMember, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewLoginTitle, FontHelper.FontType.FONT_REGULAR, this);
    }

    private void getGenders() {
        Resources res = getResources();
        String[] genders = res.getStringArray(R.array.gender);

        for (int i = 0; i < genders.length; i++) {
            String monthId = "" + i;
            String monthName = genders[i];

            HashMap hashMap = new HashMap();
            hashMap.put(Const.KEY_ID, monthId);
            hashMap.put(Const.KEY_NAME, monthName);
            arrayListGender.add(hashMap);
            setAdapter();
        }
    }


    private void setAdapter() {
        adapterSpinner = new AdapterSpinner(this, R.layout.spinner_selected_view, arrayListGender);
        spinnerGender.setAdapter(adapterSpinner);
    }

    private void getSignupDataApi() {

        String deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(this).getFcmToken();

        String firstname = String.valueOf(edtTextFirstName.getText());
        String lastName = String.valueOf(edtTextLastName.getText());
        String gender = selectedGender;
        String dob = String.valueOf(txtViewDob.getText());

        String currentAddress = String.valueOf(edtTextCurrentAdd.getText());
        String phoneNumber = String.valueOf(edtTextPhoneNo.getText());
        String email = String.valueOf(edtTextEmail.getText());
        String password = String.valueOf(edtTextPassword.getText());
        String pinCode = String.valueOf(edtTextPinCode.getText());
        String monthlySalary = String.valueOf(edtMonthlySalary.getText());
        String pancardNo = String.valueOf(edtTextPanCardNo.getText());
        String currentLatitude = PrefUtil.getString(this, ConstSession.PREF_LATITUIDE, "");
        String currentLongitude = PrefUtil.getString(this, ConstSession.PREF_LONGITUDE, "");

/*        if (validation(firstname, lastName, gender, dob, currentAddress, phoneNumber, email, password, pinCode, monthlySalary, pancardNo)) {*/

            if (validations(firstname, lastName, phoneNumber, email, password)) {
            try {
                jsonObject = new JSONObject();
                jsonObject.put(Const.PARAM_FIRST_NAME, firstname);
                jsonObject.put(Const.PARAM_LAST_NAME, lastName);
                jsonObject.put(Const.PARAM_EMAIL, email);
                jsonObject.put(Const.PARAM_PASSWORD, password);
                jsonObject.put(Const.PARAM_CONFIRM_PASSWORD, password);
                jsonObject.put(Const.PARAM_GENDER, selectedGender);
                jsonObject.put(Const.PARAM_ADDRESS, currentAddress);
                jsonObject.put(Const.PARAM_MOBILE, phoneNumber);
                //jsonObject.put(Const.PARAM_LAT, "28.535516");
                // jsonObject.put(Const.PARAM_LONG, "77.391026");
                jsonObject.put(Const.PARAM_DOB, dob);
                jsonObject.put(Const.PARAM_PIN_CODE, pinCode);
                jsonObject.put(Const.PARAM_SALARY, monthlySalary);
                jsonObject.put(Const.PARAM_PAN_CARD_NO, pancardNo);
                jsonObject.put(Const.PARAM_CURRENT_LAT, currentLatitude);
                jsonObject.put(Const.PARAM_CURRENT_LONG, currentLongitude);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            signupPresenter.getSignup(jsonObject);
        }
    }

    private boolean validation(String firstname, String lastName, String gender, String dob, String currentAddress, String phoneNumber, String email, String password, String pinCode, String monthlySalary, String pancardNo) {
        if (Utils.isEmptyOrNull(firstname)) {
            SnackNotify.showMessage(getString(R.string.empty_firstname), coordinator);
            return false;
        } else if (Utils.isEmptyOrNull(lastName)) {
            SnackNotify.showMessage(getString(R.string.empty_lastname), coordinator);
            return false;
        } else if (Utils.isEmptyOrNull(gender)) {
            SnackNotify.showMessage(getString(R.string.empty_gender), coordinator);
            return false;
        } else if (Utils.isEmptyOrNull(dob)) {
            SnackNotify.showMessage(getString(R.string.empty_dob), coordinator);
            return false;
        } else if (Utils.isEmptyOrNull(currentAddress)) {
            SnackNotify.showMessage(getString(R.string.empty_address), coordinator);
            return false;
        } else if (Utils.isEmptyOrNull(phoneNumber)) {
            SnackNotify.showMessage(getString(R.string.empty_phoneno), coordinator);
            return false;
        } else if (Utils.isEmptyOrNull(email)) {
            SnackNotify.showMessage(getString(R.string.empty_email), coordinator);
            return false;
        } else if (!Utils.isValidEmail(email)) {
            SnackNotify.showMessage(getString(R.string.error_email), coordinator);
            return false;
        } else if (Utils.isEmptyOrNull(password)) {
            SnackNotify.showMessage(getString(R.string.empty_password), coordinator);
            return false;
        } else if (!Utils.isPasswordValid(password)) {
            SnackNotify.showMessage(getString(R.string.empty_valid_password), coordinator);
            return false;
        } else if (Utils.isEmptyOrNull(pinCode)) {
            SnackNotify.showMessage(getString(R.string.empty_pin_code), coordinator);
            return false;
        } else if (Utils.isEmptyOrNull(monthlySalary)) {
            SnackNotify.showMessage(getString(R.string.empty_salary), coordinator);
            return false;
        } else if (Utils.isEmptyOrNull(pancardNo)) {
            SnackNotify.showMessage(getString(R.string.empty_pancard), coordinator);
            return false;
        }
        return true;
    }

    private boolean validations(String firstname, String lastName, String phoneNumber, String email, String password) {
        if (Utils.isEmptyOrNull(firstname)) {
            SnackNotify.showMessage(getString(R.string.empty_firstname), coordinator);
            return false;
        } else if (Utils.isEmptyOrNull(lastName)) {
            SnackNotify.showMessage(getString(R.string.empty_lastname), coordinator);
            return false;
        }else if (Utils.isEmptyOrNull(phoneNumber)) {
            SnackNotify.showMessage(getString(R.string.empty_phoneno), coordinator);
            return false;
        } else if (Utils.isEmptyOrNull(email)) {
            SnackNotify.showMessage(getString(R.string.empty_email), coordinator);
            return false;
        } else if (!Utils.isValidEmail(email)) {
            SnackNotify.showMessage(getString(R.string.error_email), coordinator);
            return false;
        } else if (Utils.isEmptyOrNull(password)) {
            SnackNotify.showMessage(getString(R.string.empty_password), coordinator);
            return false;
        } else if (!Utils.isPasswordValid(password)) {
            SnackNotify.showMessage(getString(R.string.empty_valid_password), coordinator);
            return false;
        }
        return true;
    }

    /*--------------------------------public method-------------------------------------------*/

    // setting populated date
    public void populateSetDate(int year, int month, int day) {
        showDate(year, month, day);
    }

    private void showDate(int year, int month, int day) {
        String date = new StringBuilder().append(day).append("-").append(month).append("-").append(year).toString();

        txtViewDob.setText(date);
    }

    /*----------------------------------click method---------------------------------------*/
    @OnClick(R.id.txtViewLoginTitle)
    public void ttxtViewLoginTitleClicked() {
        finish();
    }

    @OnClick(R.id.btnSignUp)
    public void btnSignUpClicked() {
        // hide keyboard
        Utils.hideKeyboardIfOpen(this);
        getSignupDataApi();
    }


    @OnClick(R.id.txtViewDob)
    public void txtViewDobSelected() {
        DialogFragment dialogFragment = new DateFragment();
        dialogFragment.show(this.getSupportFragmentManager(), "DatePcker");
    }

    @OnItemSelected(R.id.spinnerGender)
    void onItemAddressSelected(int position) {
        if (position != 0) {
            String genderId = String.valueOf(arrayListGender.get(position).get(Const.KEY_ID));
            selectedGender = String.valueOf(arrayListGender.get(position).get(Const.KEY_NAME));
            spinnerGender.setSelection(arrayListGender.indexOf(genderId));
        } else {
            selectedGender = "";
        }
    }
}
