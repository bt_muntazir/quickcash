package com.quickcash.mvp.mycash_application;

import java.util.ArrayList;

/**
 * Created by BRAINTECH on 16-01-2018.
 */

public interface MyCashApplicationView {
    public void onSuccess(ArrayList<MyCashApplicationModel.Datum> data);
    public void onUnsuccess(String message);
    public void onInternetError();

}
