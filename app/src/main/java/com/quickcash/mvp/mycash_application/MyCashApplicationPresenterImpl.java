package com.quickcash.mvp.mycash_application;

import android.app.Activity;
import android.provider.Settings;

import com.quickcash.BuildConfig;
import com.quickcash.R;
import com.quickcash.common.helpers.LoginManager;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ApiAdapter;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.session.FcmSession;
import com.quickcash.mvp.loan_packages.LoanPackagesModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by BRAINTECH on 16-01-2018.
 */

public class MyCashApplicationPresenterImpl implements MyCashApplicationPresenter {

    Activity activity;
    MyCashApplicationView myCashApplicationView;
    JSONObject jsonObject;

    public MyCashApplicationPresenterImpl(Activity activity, MyCashApplicationView myCashApplicationView) {
        this.activity = activity;
        this.myCashApplicationView = myCashApplicationView;
    }

    @Override
    public void callingMyCashApplicationApi() {
        try {
            ApiAdapter.getInstance(activity);
            gettingResult();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            myCashApplicationView.onInternetError();
        }
    }

    private void gettingResult() {
        Progress.start(activity);

        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();
        int userId= LoginManager.getInstance().getUserData().getUserId();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_ID,userId);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE, "A");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<MyCashApplicationModel> getOutput = ApiAdapter.getApiService().appliedLoans("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<MyCashApplicationModel>() {
            @Override
            public void onResponse(Call<MyCashApplicationModel> call, Response<MyCashApplicationModel> response) {
                Progress.stop();

                try {
                    //getting whole data from response
                    MyCashApplicationModel myCashApplicationModel = response.body();
                    if (myCashApplicationModel.getStatus()==(Const.PARAM_SUCCESS)) {
                        myCashApplicationView.onSuccess(myCashApplicationModel.getData());
                    } else {
                        myCashApplicationView.onUnsuccess(myCashApplicationModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    myCashApplicationView.onUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<MyCashApplicationModel> call, Throwable t) {
                Progress.stop();
                myCashApplicationView.onUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
