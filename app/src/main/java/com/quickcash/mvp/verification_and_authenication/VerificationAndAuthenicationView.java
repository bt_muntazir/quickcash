package com.quickcash.mvp.verification_and_authenication;

/**
 * Created by BRAINTECH on 17-01-2018.
 */

public interface VerificationAndAuthenicationView {
    public void onSuccessVerification(VerificationAndAuthenicationModel.Data data);
    public void onUnsuccessVerification(String message);
    public void onInternetErroVerification();

    public void onSuccessReferral(String messgae);
    public void onUnsuccessReferral(String messgae);
    public void onInternetErrorReferral();

    public void onSuccessAuthentication(String message);
    public void onUnsuccessAuthentication(String message);
    public void onInternetErrorAuthentication();
}
