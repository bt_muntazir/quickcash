package com.quickcash.mvp.verification_and_authenication;

import android.app.Activity;
import android.provider.Settings;

import com.quickcash.BuildConfig;
import com.quickcash.R;
import com.quickcash.common.helpers.LoginManager;
import com.quickcash.common.helpers.Progress;
import com.quickcash.common.requestresponse.ApiAdapter;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.requestresponse.ConstSession;
import com.quickcash.common.session.FcmSession;
import com.quickcash.common.utility.PrefUtil;
import com.quickcash.mvp.common_mvp.CommonModel;
import com.quickcash.mvp.mycash_application.MyCashApplicationModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by BRAINTECH on 17-01-2018.
 */

public class VerificationAndAuthenicationPresenterImpl implements VerificationAndAuthenicationPresenter {

    Activity activity;
    VerificationAndAuthenicationView verificationAndAuthenicationView;
    JSONObject jsonObject;

    public VerificationAndAuthenicationPresenterImpl(Activity activity,
                                                     VerificationAndAuthenicationView verificationAndAuthenicationView) {
        this.activity = activity;
        this.verificationAndAuthenicationView = verificationAndAuthenicationView;
    }

    @Override
    public void callingVerificationApi() {
        try {
            ApiAdapter.getInstance(activity);
            getVerificationApi();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            verificationAndAuthenicationView.onInternetErroVerification();
        }
    }

    private void getVerificationApi() {

        Progress.start(activity);

        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();
        int userId = LoginManager.getInstance().getUserData().getUserId();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_ID, userId);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE, "A");
            jsonObject.put(Const.PARAM_PAGE_NAME, "term_conditions");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<VerificationAndAuthenicationModel> getOutput = ApiAdapter.getApiService().verification("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<VerificationAndAuthenicationModel>() {
            @Override
            public void onResponse(Call<VerificationAndAuthenicationModel> call, Response<VerificationAndAuthenicationModel> response) {
                Progress.stop();

                try {
                    VerificationAndAuthenicationModel verificationAndAuthenicationModel = response.body();
                    if (verificationAndAuthenicationModel.getStatus() == (Const.PARAM_SUCCESS)) {
                        verificationAndAuthenicationView.onSuccessVerification(verificationAndAuthenicationModel.getData());
                    } else {
                        verificationAndAuthenicationView.onUnsuccessVerification(verificationAndAuthenicationModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    verificationAndAuthenicationView.onUnsuccessVerification(activity.getString(R.string.error_server));
                }

            }

            @Override
            public void onFailure(Call<VerificationAndAuthenicationModel> call, Throwable t) {
                Progress.stop();
                verificationAndAuthenicationView.onUnsuccessVerification(activity.getString(R.string.error_server));
            }
        });

    }

    @Override
    public void callingReferralApi(String referral) {
        try {
            ApiAdapter.getInstance(activity);
            getReferralApiResult(referral);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            verificationAndAuthenicationView.onInternetErrorReferral();
        }
    }


    private void getReferralApiResult(String referral) {
        Progress.start(activity);

        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();
        int userId = LoginManager.getInstance().getUserData().getUserId();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_ID, userId);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE, "A");
            jsonObject.put(Const.PARAM_CODE, referral);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getOutput = ApiAdapter.getApiService().referral("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                Progress.stop();
                try {
                    CommonModel commonModel = response.body();
                    if (commonModel.getStatus() == (Const.PARAM_SUCCESS)) {
                        verificationAndAuthenicationView.onSuccessReferral(commonModel.getMessage());
                    } else {
                        verificationAndAuthenicationView.onUnsuccessReferral(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    verificationAndAuthenicationView.onUnsuccessReferral(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                verificationAndAuthenicationView.onUnsuccessReferral(activity.getString(R.string.error_server));
            }
        });
    }


    @Override
    public void callingAuthenicationApi(int interestId, int loanId, String date) {
        try {
            ApiAdapter.getInstance(activity);
            getAuthenicationApi(interestId, loanId, date);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            verificationAndAuthenicationView.onInternetErrorAuthentication();
        }
    }

    private void getAuthenicationApi(int interestId, int loadId, String date) {
        Progress.start(activity);

        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();
        int userId = LoginManager.getInstance().getUserData().getUserId();
        String currentLatitude = PrefUtil.getString(activity, ConstSession.PREF_LATITUIDE, "");
        String currentLongitude = PrefUtil.getString(activity, ConstSession.PREF_LONGITUDE, "");


        try {
            jsonObject = new JSONObject();

            jsonObject.put(Const.PARAM_USER_ID, userId);
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId);
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.PARAM_DEVICE_TYPE, "A");
            jsonObject.put(Const.PARAM_CURRENT_LAT, currentLatitude);
            jsonObject.put(Const.PARAM_CURRENT_LONG, currentLongitude);
            jsonObject.put(Const.PARAM_INTEREST_ID, interestId);
            jsonObject.put(Const.PARAM_LOAN_ID, loadId);
            jsonObject.put(Const.PARAM_APPLIED_DATE, date);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getOutput = ApiAdapter.getApiService().authentication("application/json", "no-cache", body);
        getOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                Progress.stop();
                try {
                    CommonModel commonModel = response.body();
                    if (commonModel.getStatus()==(Const.PARAM_SUCCESS)) {
                        verificationAndAuthenicationView.onSuccessAuthentication(commonModel.getMessage());
                    } else {
                        verificationAndAuthenicationView.onUnsuccessAuthentication(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    verificationAndAuthenicationView.onUnsuccessAuthentication(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                verificationAndAuthenicationView.onUnsuccessAuthentication(activity.getString(R.string.error_server));
            }
        });
    }

}
