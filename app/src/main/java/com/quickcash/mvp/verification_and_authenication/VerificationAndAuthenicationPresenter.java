package com.quickcash.mvp.verification_and_authenication;

/**
 * Created by BRAINTECH on 17-01-2018.
 */

public interface VerificationAndAuthenicationPresenter {
    public void callingVerificationApi();

    public void callingReferralApi(String referral);

    public void callingAuthenicationApi(int interestId, int loadId, String date);
}
