package com.quickcash.mvp.verification_and_authenication;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.AsyncTask;
import android.provider.SyncStateContract;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.safetynet.SafetyNetClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.quickcash.R;
import com.quickcash.common.helpers.AlertDialogHelper;
import com.quickcash.common.helpers.FontHelper;
import com.quickcash.common.interfaces.OnClickInterface;
import com.quickcash.common.requestresponse.Const;
import com.quickcash.common.requestresponse.ConstIntent;
import com.quickcash.common.utility.SnackNotify;
import com.quickcash.common.utility.Utils;
import com.quickcash.mvp.navigation.NavigationActivity;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerificationAndAuthenicationActivity extends AppCompatActivity implements VerificationAndAuthenicationView {

    final int MAXIMUM_BIT_LENGTH = 100;
    final int RADIX = 32;

    String token;
    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.txtViewTitleEnteReferCode)
    TextView txtViewTitleEnteReferCode;

    @BindView(R.id.txtViewHiThere)
    TextView txtViewHiThere;

    @BindView(R.id.edtTextReferralCode)
    EditText edtTextReferralCode;

    @BindView(R.id.btnApply)
    Button btnApply;

    @BindView(R.id.txtViewTitleTermAndConditions)
    TextView txtViewTitleTermAndConditions;

    @BindView(R.id.txtViewTermAndConditions)
    TextView txtViewTermAndConditions;

    @BindView(R.id.checkbox)
    CheckBox checkbox;

    @BindView(R.id.txtViewTerms)
    TextView txtViewTerms;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @BindView(R.id.txtViewPoweredBy)
    TextView txtViewPoweredBy;

    @BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;

    @BindView(R.id.imgViewBox)
    ImageView imgViewBox;

    @BindView(R.id.txtViewI_AM_NOT_ROBOT)
    TextView txtViewI_AM_NOT_ROBOT;

    @BindView(R.id.txtViewReCaptcha)
    TextView txtViewReCaptcha;

    @BindView(R.id.txtViewPrivacy)
    TextView txtViewPrivacy;

    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;

    boolean flag;

    private static final String PUBLIC_KEY = "6LcPWugSAAAAAC-MP5sg6wp_CQiyxHvPvkQvVlVf";
    private static final String PRIVATE_KEY = "6LcPWugSAAAAALWMp-gg9QkykQQyO6ePBSUk-Hjg";
    private EditText answer;

    VerificationAndAuthenicationPresenterImpl verificationAndAuthenicationPresenterImpl;

    String referralCode;

    Calendar calendar;
    DatePickerDialog fromDatePickerDialog = null;
    private SimpleDateFormat dateFormatter;
    String currentDate;
    int tenureInterestId = -1;
    int loanId = -1;

    SafetyNetClient myClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_and_authenication);
        ButterKnife.bind(this);
        nestedScrollView.setNestedScrollingEnabled(true);
        myClient = SafetyNet.getClient(this);
        setFont();
        getCurrentDate();
        getIntentData();
        verificationAndAuthenicationPresenterImpl = new VerificationAndAuthenicationPresenterImpl(this, this);
        verificationApi();
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewTitleEnteReferCode, FontHelper.FontType.FONT_MEDIUM);
        FontHelper.applyFont(this, txtViewHiThere, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, edtTextReferralCode, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, btnApply, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewTitleTermAndConditions, FontHelper.FontType.FONT_BOLD);
        FontHelper.applyFont(this, txtViewTermAndConditions, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewTerms, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewI_AM_NOT_ROBOT, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewReCaptcha, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, txtViewPrivacy, FontHelper.FontType.FONT_REGULAR);
        FontHelper.applyFont(this, btnSubmit, FontHelper.FontType.FONT_REGULAR);
    }

    private void verificationApi() {
        verificationAndAuthenicationPresenterImpl.callingVerificationApi();
    }


    @Override
    public void onSuccessVerification(VerificationAndAuthenicationModel.Data data) {
        txtViewTitleTermAndConditions.setText(data.getTitle());
        txtViewTermAndConditions.setText(data.getDescription());
    }

    @Override
    public void onUnsuccessVerification(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void onInternetErroVerification() {
        SnackNotify.checkConnection(onRetryVerification, relLayMain);
    }

    OnClickInterface onRetryVerification = new OnClickInterface() {
        @Override
        public void onClick() {
            verificationApi();
        }
    };

    @Override
    public void onSuccessReferral(String message) {
        edtTextReferralCode.setText("");
        edtTextReferralCode.setEnabled(false);
        btnApply.setText("Applied");
        btnApply.setAlpha(0.5f);
        btnApply.setEnabled(false);
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void onUnsuccessReferral(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void onInternetErrorReferral() {
        SnackNotify.checkConnection(onRetryReferral, relLayMain);
    }

    OnClickInterface onRetryReferral = new OnClickInterface() {
        @Override
        public void onClick() {
            referralApi();
        }
    };

    @Override
    public void onSuccessAuthentication(String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(VerificationAndAuthenicationActivity.this, NavigationActivity.class);
                startActivity(intent);
                finishAffinity();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onUnsuccessAuthentication(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void onInternetErrorAuthentication() {
        SnackNotify.checkConnection(onRetryAuthentication, relLayMain);
    }

    OnClickInterface onRetryAuthentication = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };


    @OnClick(R.id.btnApply)
    public void btnApplyClick() {
        referralApi();
    }

    private void referralApi() {
        Utils.hideKeyboardIfOpen(this);
        referralCode = edtTextReferralCode.getText().toString();
        verificationAndAuthenicationPresenterImpl.callingReferralApi(referralCode);
    }


    @OnClick(R.id.btnSubmit)
    public void btnSubmitClick() {
        getData();
    }

    private void getData() {
        if (checkbox.isChecked()) {
            if(flag){
                verificationAndAuthenicationPresenterImpl.callingAuthenicationApi(tenureInterestId, loanId, currentDate);
            }else{
                onUnsuccessAuthentication("Please confirm you are human.");
            }
        } else {
            onUnsuccessAuthentication("Please select terms and conditions.");
        }

    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getCurrentDate() {
        calendar = Calendar.getInstance();
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        currentDate = (dateFormatter.format(calendar.getTime()));
    }

    private void getIntentData() {
        if (getIntent() != null) {
            loanId = getIntent().getIntExtra(ConstIntent.KEY_LOAN_ID, -1);
            tenureInterestId = getIntent().getIntExtra(ConstIntent.KEY_TENURE_INTEREST_ID, -1);
            Log.e("loanid", "" + loanId);
            Log.e("interestd", "" + tenureInterestId);
        }
    }

    @OnClick(R.id.relativeLayout)
    public void relativeLayoutClick(View v) {

        SafetyNet.getClient(this).verifyWithRecaptcha("6Le23kEUAAAAAKzVucQ1PnbMN7UGu6xTsBfTudRt")
                .addOnSuccessListener(this, new OnSuccessListener<SafetyNetApi.RecaptchaTokenResponse>() {
                    @Override
                    public void onSuccess(SafetyNetApi.RecaptchaTokenResponse response) {
                        if (!response.getTokenResult().isEmpty()) {
                            //handleSiteVerify(response.getTokenResult());
                            token = response.getTokenResult();
                            Log.e("token", token);
                            new ApiCalling().execute();
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            Log.d("", "Error message: " +
                                    CommonStatusCodes.getStatusCodeString(apiException.getStatusCode()));
                        } else {
                            Log.d("", "Unknown type of error: " + e.getMessage());
                        }
                    }
                });

    }


    class ApiCalling extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {

        }


        @Override
        protected String doInBackground(Void... voids) {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("secret", "6Le23kEUAAAAAEGpGj1u756gFpACrmHmCDF7caji");
                jsonObject.put("response", token);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                // Creating & connection Connection with url and required Header.
                URL url = new URL("https://www.google.com/recaptcha/api/siteverify");
                HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                urlConnection.setRequestMethod("POST");   //POST or GET
                urlConnection.connect();

                // Write Request to output stream to server.
                OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
                out.write("secret=" + "6Le23kEUAAAAAEGpGj1u756gFpACrmHmCDF7caji" + "&response=" + token);
                out.close();

                // Check the connection status.
                int statusCode = urlConnection.getResponseCode();
                String statusMsg = urlConnection.getResponseMessage();

                // Connection success. Proceed to fetch the response.
                if (statusCode == 200) {
                    InputStream it = new BufferedInputStream(urlConnection.getInputStream());
                    InputStreamReader read = new InputStreamReader(it);
                    BufferedReader buff = new BufferedReader(read);
                    StringBuilder dta = new StringBuilder();
                    String chunks;
                    while ((chunks = buff.readLine()) != null) {
                        dta.append(chunks);
                    }
                    String returndata = dta.toString();

                    Log.e("returndata", "-------->" + returndata);

                    return returndata;
                } else {
                    //Handle else case
                }
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            if (response != null) {
                Log.e("response", "----------->" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                     flag = (boolean) jsonObject.get("success");
                    if (flag) {
                        imgViewBox.setImageResource(R.mipmap.ic_box_filled);
                        imgViewBox.setColorFilter(ContextCompat.getColor(VerificationAndAuthenicationActivity.this, R.color.alpha_black));
                        relativeLayout.setEnabled(false);
                    } else {
                        imgViewBox.setImageResource(R.mipmap.ic_box);
                        imgViewBox.setColorFilter(ContextCompat.getColor(VerificationAndAuthenicationActivity.this, R.color.alpha_black));
                        relativeLayout.setEnabled(true);
                    }
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }


    }
}
