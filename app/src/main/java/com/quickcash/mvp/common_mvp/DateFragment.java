package com.quickcash.mvp.common_mvp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.quickcash.R;
import com.quickcash.mvp.profile.ProfileFragment;
import com.quickcash.mvp.signup.SignupActivity;

import java.util.Calendar;

/**
 * Created by Braintech on 15-01-2018.
 */

public class DateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private final String TAG_DIALOG_FRAGMENT = "dialogFragment";
    DatePickerDialog datePickerDialog;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);


        if (getActivity() instanceof SignupActivity) {
            datePickerDialog = new DatePickerDialog(getActivity(), this, yy, mm, dd);
        } else {

            datePickerDialog = new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        //calendar.add(Calendar.MONTH, 1); // add 1 Month to max date from now
        datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        //datePickerDialog.getDatePicker().setMaxDate(now+(1000*60*60*24*20)); //After 20 Days from Now

        return datePickerDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int yy, int mm, int dd) {

        if (getActivity() instanceof SignupActivity) {
            ((SignupActivity) getActivity()).populateSetDate(yy, mm + 1, dd);
        } else {
            ProfileFragment profileFragment = (ProfileFragment) getFragmentManager().findFragmentByTag(getString(R.string.nav_myprofile));
            profileFragment.populateSetDate(yy, mm + 1, dd);
        }
    }
}
