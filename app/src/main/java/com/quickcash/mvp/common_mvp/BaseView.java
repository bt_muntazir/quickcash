package com.quickcash.mvp.common_mvp;

/**
 * Created by Braintech on 12-01-2018.
 */

public interface BaseView {

    void onError(String message);

    void onInternetError();

    void apiUnsuccess(String message);

    void serverNotResponding(String message);
}