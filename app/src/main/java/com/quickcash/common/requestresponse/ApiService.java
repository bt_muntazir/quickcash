package com.quickcash.common.requestresponse;

import com.quickcash.mvp.applied_loan_detail.AppliedLoanDetailModel;
import com.quickcash.mvp.bank_account.BankAccountModel;
import com.quickcash.mvp.common_mvp.CommonModel;
import com.quickcash.mvp.disbursement.DisbursementModel;
import com.quickcash.mvp.loan_detail.LoanDetailModel;
import com.quickcash.mvp.loan_packages.LoanPackagesModel;
import com.quickcash.mvp.login.model.LoginResponseModel;
import com.quickcash.mvp.mycash_application.MyCashApplicationModel;
import com.quickcash.mvp.profile.ProfileModel;
import com.quickcash.mvp.verification_and_authenication.VerificationAndAuthenicationModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Braintech on 11-01-2018.
 */

public interface ApiService {

    //register Api
    @POST("api/users/user-register.json")
    Call<CommonModel> signup(@Header("Content-Type")
                                     String contentType,
                             @Header("Cache-Control")
                                     String cache,
                             @Body RequestBody params);

    //login Api
    @POST("api/users/login.json")
    Call<LoginResponseModel> login(@Header("Content-Type")
                                           String contentType,
                                   @Header("Cache-Control")
                                           String cache,
                                   @Body RequestBody params);

    //Change password Api
    @POST("api/users/change-password.json")
    Call<CommonModel> changePassword(@Header("Content-Type")
                                           String contentType,
                                   @Header("Cache-Control")
                                           String cache,
                                   @Body RequestBody params);

    //logout Api
    @POST("api/users/logout.json")
    Call<CommonModel> logout(@Header("Content-Type")
                                     String contentType,
                             @Header("Cache-Control")
                                     String cache,
                             @Body RequestBody params);


    //forgot password Api
    @POST("api/users/forgot-password.json")
    Call<CommonModel> forgotPassword(@Header("Content-Type")
                                             String contentType,
                                     @Header("Cache-Control")
                                             String cache,
                                     @Body RequestBody params);


    //get user profile Api
    @POST("api/users/user-profile.json")
    Call<ProfileModel> userProfile(@Header("Content-Type")
                                           String contentType,
                                   @Header("Cache-Control")
                                           String cache,
                                   @Body RequestBody params);


    //update user profile Api
    @POST("api/users/user-profile-update.json")
    Call<CommonModel> updateProfile(@Header("Content-Type")
                                            String contentType,
                                    @Header("Cache-Control")
                                            String cache,
                                    @Body RequestBody params);


    @POST("api/loans/loan-packages.json")
    Call<LoanPackagesModel> loanPackages(@Header("Content-Type")
                                                 String contentType,
                                         @Header("Cache-Control")
                                                 String cache,
                                         @Body RequestBody params);


    @POST("api/loans/users-applied-loans.json")
    Call<MyCashApplicationModel> appliedLoans(@Header("Content-Type")
                                                      String contentType,
                                              @Header("Cache-Control")
                                                      String cache,
                                              @Body RequestBody params);

    //Add update bank account Api
    @POST("api/users/user-add-bank-account.json")
    Call<CommonModel> saveBankAccount(@Header("Content-Type")
                                              String contentType,
                                      @Header("Cache-Control")
                                              String cache,
                                      @Body RequestBody params);


    @POST("api/users/bank-account-details.json")
    Call<BankAccountModel> getBankAccount(@Header("Content-Type")
                                                  String contentType,
                                          @Header("Cache-Control")
                                                  String cache,
                                          @Body RequestBody params);

    //loan detail Api
    @POST("api/loans/loan-details.json")
    Call<LoanDetailModel> loanDetail(@Header("Content-Type")
                                             String contentType,
                                     @Header("Cache-Control")
                                             String cache,
                                     @Body RequestBody params);

    //Payment Modes List
    @POST("api/loans/payment-modes.json")
    Call<DisbursementModel> paymentModeList(@Header("Content-Type")
                                               String contentType,
                                            @Header("Cache-Control")
                                               String cache,
                                            @Body RequestBody params);

    //upload User's document
    @Multipart
    @POST("api/users/users-documents.json")
    Call<CommonModel> uploadDocument(@Part MultipartBody.Part photo,
                                      @Part MultipartBody.Part pancard,
                                      @Part MultipartBody.Part bankStatement,
                                      @Part MultipartBody.Part addressProof,
                                      @Part(Const.PARAM_USER_ID) RequestBody userId,
                                      @Part(Const.PARAM_LOAN_ID) RequestBody loan,
                                      @Part(Const.PARAM_DEVICE_ID) RequestBody deviceId,
                                      @Part(Const.PARAM_DEVICE_TOKEN) RequestBody deviceToken,
                                      @Part(Const.PARAM_DEVICE_TYPE) RequestBody deviceType);



    @POST("api/cms/content.json")
    Call<VerificationAndAuthenicationModel> verification(@Header("Content-Type")
                                                    String contentType,
                                                         @Header("Cache-Control")
                                                    String cache,
                                                         @Body RequestBody params);

    @POST("api/loans/validate-referrals.json")
    Call<CommonModel> referral(@Header("Content-Type")
                                            String contentType,
                                    @Header("Cache-Control")
                                            String cache,
                                    @Body RequestBody params);

    @POST("api/loans/apply-loan.json")
    Call<CommonModel> authentication(@Header("Content-Type")
                                       String contentType,
                               @Header("Cache-Control")
                                       String cache,
                               @Body RequestBody params);



    //loan detail Api
    @POST("api/loans/users-applied-loan-details.json")
    Call<AppliedLoanDetailModel> appliedLoanDetail(@Header("Content-Type")
                                             String contentType,
                                                   @Header("Cache-Control")
                                             String cache,
                                                   @Body RequestBody params);


    //Save Disbursement type
    @POST("api/loans/save-payment-mode.json")
    Call<CommonModel> saveDisbursementtype(@Header("Content-Type")
                                                    String contentType,
                                            @Header("Cache-Control")
                                                    String cache,
                                            @Body RequestBody params);



    //upload User's document
    /*@Multipart
    @POST("api/users/users-documents.json")
    Call<ResponseBody> uploadDocument(@Part MultipartBody.Part photo,
                                      @Part(Const.PARAM_USER_ID) RequestBody userId,
                                      @Part(Const.PARAM_LOAN_ID) RequestBody loan,
                                      @Part(Const.PARAM_DEVICE_ID) RequestBody deviceId,
                                      @Part(Const.PARAM_DEVICE_TOKEN) RequestBody deviceToken,
                                      @Part(Const.PARAM_DEVICE_TYPE) RequestBody deviceType);*/

}

