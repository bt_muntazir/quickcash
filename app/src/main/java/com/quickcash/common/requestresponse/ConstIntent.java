package com.quickcash.common.requestresponse;

/**
 * Created by Braintech on 11-01-2018.
 */

public class ConstIntent {

    public static String KEY_LOAN_ID = "loan_id";

    public static String KEY_DATA="data";
    public static String KEY_TENURE_INTEREST_ID="tenure_interest_id";
}
