package com.quickcash.common.requestresponse;

/**
 * Created by Braintech on 11-01-2018.
 */

public class Const {

    public static final String Base_URL = "http://braintechnosys.net/loan_app/";

    public static final String APP_KEY = "APPKEY";
    public static final String APP_KEY_VALUE = "123456789";
    public static final int PARAM_SUCCESS = 1;
    public static final String PARAM_ANDROID = "A";

    //Hashmao key
    public static String KEY_ID = "id";
    public static String KEY_NAME = "name";
    public static String KEY_PRICE_SYMBOL = "Php";
    public static String KEY_LOAN_ID = "loan_id";


    public static String KEY_NEW = "New";
    public static String KEY_REJECTED = "Rejected";
    public static String KEY_APPROVED = "Approved";
    public static String KEY_ACTIVE = "Active";
    public static String KEY_INACTIVE = "Inactive";
    public static String KEY_PAID = "Paid";


    /*login*/
    public static final String PARAM_LOAN_ID = "loan_id";
    public static final String PARAM_PAYMENT_MODE_ID = "payment_mode_id";
    public static final String PARAM_USER_LOAN_ID = "user_loan_id";
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_DEVICE_ID = "device_id";
    public static final String PARAM_DEVICE_TOKEN = "device_token";
    public static final String PARAM_DEVICE_TYPE = "device_type";
    public static final String PARAM_PAGE_NAME = "page_name";
    public static final String PARAM_CODE = "code";



    /*bank detail*/

    public static final String PARAM_ACCOUNT_TYPE = "account_type";
    public static final String PARAM_ACCOUNT_NO = "account_no";
    public static final String PARAM_IFSC_CODE = "ifsc_code";
    public static final String PARAM_BANK_NAME = "bank_name";

    /*Register*/
    public static final String PARAM_FIRST_NAME = "first_name";
    public static final String PARAM_LAST_NAME = "last_name";
    public static final String PARAM_MOBILE = "mobile";
    public static final String PARAM_GENDER = "gender";
    public static final String PARAM_ADDRESS = "address";
    public static final String PARAM_LAT = "lat";
    public static final String PARAM_LONG = "long";
    public static final String PARAM_PIN_CODE = "pin_code";
    public static final String PARAM_DOB = "dob";
    public static final String PARAM_PAN_CARD_NO = "pan_card_no";
    public static final String PARAM_SALARY = "salary";
    public static final String PARAM_CURRENT_LAT = "current_lat";
    public static final String PARAM_CURRENT_LONG = "current_long";

    public static final String PARAM_TENURE_INTERESTID = "tenure_interest_id";


    /*forgot password*/
    public static final String PARAM_OLD_PASSWORD = "old_password";
    public static final String PARAM_CONFIRM_PASSWORD = "confirm_password";


    /*Upload document password*/
    public static final String PARAM_PHOTO = "photo";
    public static final String PARAM_PAN_CARD = "pan_card";
    public static final String PARAM_BANK_STATEMENT = "bank_statement";
    public static final String PARAM_ADDRESS_PROOF = "address_proof";
    public static final String PARAM_INTEREST_ID = "tenure_interest_id";
    public static final String PARAM_APPLIED_DATE = "applied_date";


}
