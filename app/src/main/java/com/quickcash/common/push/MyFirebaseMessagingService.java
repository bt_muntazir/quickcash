package com.quickcash.common.push;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import com.quickcash.R;
import com.quickcash.common.session.FcmSession;
import com.quickcash.mvp.login.LoginActivity;

import java.util.Map;

/**
 * Created by Braintech on 8/8/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {


    //UserSession userSession;

    int sound = 0;
    int playSound;
    int notificationType = 0;
    FcmSession fcmSession;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            fcmSession = new FcmSession(this);

            //Calling method to generate notification
            sendNotification(remoteMessage.getNotification().getBody());

        } catch (NullPointerException ee) {
            ee.printStackTrace();

        }
    }

    //This method is only generating push notification
    //It is same as we did in earlier posts

    private void sendNotification(String data) {

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher))
                .setContentTitle(getString(R.string.app_name))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(data))
                .setContentText(data)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1234, notificationBuilder.build());
    }
}



