package com.quickcash.common.interfaces;

/**
 * Created by Braintech on 11-01-2018.
 */

public interface OnClickInterface {
    public void onClick();
}
