package com.quickcash.common.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Braintech on 16-01-2018.
 */

public class DateFormat {

    public static String dd_mm_yyyy(String dateOfWeight) {
        String dateToShow = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+hh:mm");
            Date date = null;
            try {
                date = simpleDateFormat.parse(dateOfWeight);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            SimpleDateFormat newFormatDate = new SimpleDateFormat("dd/MM/yyyy");
            dateToShow = newFormatDate.format(date);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return dateToShow;
    }
}