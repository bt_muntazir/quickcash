package com.quickcash.common.helpers;

import com.google.gson.Gson;
import com.quickcash.common.application.QuickCashApplication;
import com.quickcash.common.utility.PrefUtil;
import com.quickcash.mvp.login.model.LoginResponseModel;

/**
 * Created by Braintech on 11-01-2018.
 */

public class LoginManager {

    public static String KEY_USER_DATA = "user_data";
    public static String IS_LOGIN = "is_login";


    private static volatile LoginManager instance;

    public static LoginManager getInstance() {
        if (instance == null) {
            synchronized (LoginManager.class) {
                if (instance == null) {
                    instance = new LoginManager();
                }
            }
        }
        return instance;
    }

    public void createLoginSession(LoginResponseModel.Data data) {
        PrefUtil.putBoolean(QuickCashApplication.getInstance(), IS_LOGIN, true);
        PrefUtil.putString(QuickCashApplication.getInstance(), KEY_USER_DATA, new Gson().toJson(data));

    }
    public boolean isLoggedIn() {
        return PrefUtil.getBoolean(QuickCashApplication.getInstance(), IS_LOGIN, false);
    }

    public LoginResponseModel.Data getUserData() {
        LoginResponseModel.Data data = new Gson().fromJson(PrefUtil.getString(QuickCashApplication.getInstance(), KEY_USER_DATA, ""), LoginResponseModel.Data.class);
        return data;
    }

    public void logoutUser() {
        PrefUtil.remove(QuickCashApplication.getInstance(), KEY_USER_DATA);
        PrefUtil.remove(QuickCashApplication.getInstance(), IS_LOGIN);
    }

}
